

/**
    這題用 slide window

    用法是：
    0, 初始起點終點都是 0
    1, 用 unordered_map 紀錄有哪些字
    2, 結束條件是窗口右邊超出 string s 範圍
    3, 每次都先檢查 s[j] 有沒有在 umap 裡面
        a, 沒有：s[i] ~ s[j] 可以繼續擴大，j++
        b, 有　：那就是窗口要縮小，那 i 該怎麼調？已知道 s[i] ~ s[j] 中間有一個 char 跟 s[j] 相同
        　　　　　如果把 i,j 都調成j那就可以繼續檢查，但這樣跳過了非常多該查的開頭
                "abcbet" 此字串就會沒找到。
                因此只需要以 "s[i] ~ s[j] 中不要有 s[j] 重複" 為目標做最低節度調整 i
                => 每次只把 i 調整成 i++;
                但這樣條會出現的情況是 s[j] 依然重複
                => 下一次 while 會繼續跑到這邊，到沒有重複出現 s[j] 就會讓 j 繼續往前
 */

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int i, j, n = s.length(), ans;
        if (n <= 1) return n;

        unordered_map<char, int> umap;
        i = j = ans = 0;

        while (j < n) {
            if (umap[s[j]] == 0) {
                umap[s[j]] = 1;
                ans = max(j-i+1, ans);
                j++;
            } else {
                umap[s[i++]] = 0;
            }
        }
        
        return ans;
    }
};