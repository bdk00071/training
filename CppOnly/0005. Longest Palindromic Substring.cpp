/**
        for loop 把 s 跑一輪
        以 idx i 當作中心，左右推出去看是不是 Palindromic Substring
        是的話更新資料
        會出現上下兩個是因為要看這字串的長度是奇數還是偶數
 */


 
class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.length(), start = 0, maxLen = 1, right, left, len;

        for (int i = 0; i < n; i++) {
            left = i;
            right = i;
            while (left >= 0 && right < n && s[left] == s[right]) {
                len = right - left + 1;
                if (len > maxLen) {
                    maxLen = len;
                    start = left;
                }
                left--;
                right++;
            }

            left = i;
            right = i + 1;
            while (left >= 0 && right < n && s[left] == s[right]) {
                len = right - left + 1;
                if (len > maxLen) {
                    maxLen = len;
                    start = left;
                }
                left--;
                right++;
            }
        }

        return s.substr(start, maxLen);
    }
};
