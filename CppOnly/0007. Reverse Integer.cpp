class Solution {
public:
    int reverse(int x) {
        bool bNegative = false;
        long long nRes = 0, tmp = (long long)x;

        if (tmp < 0) {
            bNegative = true;
            tmp = -1 * tmp;
        }

        while (tmp) {
            nRes = nRes*10 + tmp%10;
            tmp /= 10;
        }
        
        if (bNegative == true) nRes =  -1*nRes;
        if (nRes > INT_MAX || nRes < INT_MIN) nRes = 0;
        return (int)nRes;
    }
};