/*
    包兩層 for loop 找的速度太慢
    slide window 的特性是 "調 ptr 找答案"
    我們控制 i, j 獲得 height[i], height[j], j-i 然後算面積
    算好這次的之後怎麼移動 index ? 想讓面積變大那長寬得變高
    j-i 已經是用大的 value 開始跑，那有機會的已經剩下高度
    別忘了剛剛算的水體積是用 minimum
    這會造成如果我們現在想移動比較大的數字
    新的體積只可能變小
    想要更大的體積只能移動比較小的 height[i], height[j]
*/


class Solution {
public:
    int maxArea(vector<int>& height) {
        int ans = 0, i=0, j=height.size()-1;

        while (i<=j) {
            if (min(height[i], height[j]) * (j-i) > ans) {
                ans = min(height[i], height[j]) * (j-i);
            }
            
            if (height[i] >= height[j]) j--;
            else i++;
        }

        return ans;
    }
};
