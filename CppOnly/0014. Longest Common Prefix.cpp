
// 土法煉鋼

class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        int n=strs.size();

        for (int i=0; i<strs[0].length(); i++) {
            for (int j=1; j<n; j++) {
                if (strs[j][i] != strs[0][i]) {
                    if (i!=0) return strs[0].substr(0, i);
                    else return "";
                }
            }
        }

        return strs[0];
    }
};
