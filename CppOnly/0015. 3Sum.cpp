/*
    for loop *3 可以解決，但 slide window 真的很好用
    for loop 先選好三個數中最小的 nums[i] ，slide window 滑後面是否有組合能跟 nums[i] 相加等於 target
    => O(n^3) 降低成 O(n^2)
*/

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        vector<vector<int>> ans;
        int n=nums.size(), j, k;
        sort(nums.begin(), nums.end());
        for (int i=0;i<n-2;i++) {
            if (i!=0 && nums[i] == nums[i-1]) continue;
            j=i+1, k=n-1;
            while (j < k && j<n && 0<=k) {
                if (nums[i] + nums[j] + nums[k] == 0) {
                    ans.push_back({nums[i], nums[j], nums[k]});
                    j++;
                    while (j<n && nums[j-1] == nums[j]) j++;
                } else if (nums[i] + nums[j] + nums[k] < 0) {
                    j++;
                    while (j<n && nums[j-1] == nums[j]) j++;
                } else {
                    k--;
                    while (0<=k && nums[k+1] == nums[k]) k--;
                }
            }
        }
        return ans;
    }
};
