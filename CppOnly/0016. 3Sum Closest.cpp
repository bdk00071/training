
// 跟前面 3Sum 類似解法，只是多算 diff

class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        int n=nums.size(), j, k, diff = INT_MAX, ans;
        sort(nums.begin(), nums.end());
        for (int i=0;i<n-2;i++) {
            if (i!=0 && nums[i] == nums[i-1]) continue;
            j=i+1, k=n-1;
            while (j < k && j<n && 0<=k) {
                if (diff > abs(nums[i] + nums[j] + nums[k] - target)) {
                    ans = nums[i] + nums[j] + nums[k];
                    diff = abs(nums[i] + nums[j] + nums[k] - target);
                }
                if (nums[i] + nums[j] + nums[k] - target < 0) {
                    j++;
                    while (j<n && nums[j-1] == nums[j]) j++;
                } else {
                    k--;
                    while (0<=k && nums[k+1] == nums[k]) k--;
                }
            }
        }
        return ans;
    }
};
