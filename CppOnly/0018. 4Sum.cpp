// 跟 3 Sum 一樣用 slide window 找

class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        vector<vector<int>> ans;
        int n = nums.size(), left, right;
        long long newTarget;

        if (n <= 3) return ans;
        sort(nums.begin(), nums.end());

        // 固定兩個小的後, 另外兩個用 slide window 找
        for (int i=0; i<n-3; i++) {
            if (i!=0 && nums[i]==nums[i-1]) continue;
            for (int j=i+1; j<n-2; j++) {
                if (j!=i+1 && nums[j]==nums[j-1]) continue;
                newTarget = (long long)target - (long long)nums[i] - (long long)nums[j];
                left = j+1;
                right = n-1;
                while (left < right) {
                    if (nums[left] + nums[right] == newTarget) {
                        ans.push_back({nums[i], nums[j], nums[left], nums[right]});
                        left++;
                        right--;
                        while(left < right && nums[left] == nums[left-1]) left++;
                        while(left < right && nums[right] == nums[right+1]) right--;
                    } else if (nums[left] + nums[right] > newTarget) {
                        right--;
                    } else if (nums[left] + nums[right] < newTarget) {
                        left++;
                    }
                }
            }
        }

        return ans;
    }
};