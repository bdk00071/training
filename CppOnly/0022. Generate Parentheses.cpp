/*
    剛看到時會想用 dymmic programming 寫：一個 in n=4 的可以拿 n=3 的資料生出來。
    但 "(())(())" 這個字串在設計的方法不能用 n=3 的資料生出
    依然回到 backtrack 的方式
*/

class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<string> ans;
        generate(n, n, "", ans);

        return ans;
    }

    void generate(int L, int R, string tmp, vector<string>& ans) {
        if (R==0 && L==0) {
            ans.push_back(tmp);
            return;
        } else if (L==0) {
            generate(L, R-1, tmp+")", ans);
        } else if (R==L) {
            generate(L-1, R, tmp+"(", ans);
        } else {
            generate(L, R-1, tmp+")", ans);
            generate(L-1, R, tmp+"(", ans);
        }
        
        return;
    }
};
