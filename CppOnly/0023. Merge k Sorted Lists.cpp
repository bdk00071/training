
/*
    有 k 個 sorted list 要合併，比較好的做法是底下這種一次合併兩條 lists 最後剩下一條就是答案

    速度沒很快但不需要太多額外空間。

    merge 速度應該可以再加強
*/


class Solution {
private:
    ListNode *dummyM, *current;
    ListNode* merge(ListNode* list1, ListNode* list2) {
        this->dummyM->next = NULL;
        current = this->dummyM;

        if (!list1) return list2;
        if (!list2) return list1;

        while (list1 || list2) {
            if (!list1) {
                current->next = list2;
                list2 = NULL;
            } else if (!list2) {
                current->next = list1;
                list1 = NULL;
            } else if (list1->val <= list2->val) {
                current->next = list1;
                list1 = list1->next;
                current = current->next;
                current->next = NULL;
            } else {
                current->next = list2;
                list2 = list2->next;
                current = current->next;
                current->next = NULL;
            }
        }

        return this->dummyM->next;
    }
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        int nSize = lists.size();
        if (nSize == 0) return NULL;

        this->dummyM = new ListNode(0);
        for (int i=1; i<nSize; i++) {
            lists[0] = merge(lists[0], lists[i]);
        }

        return lists[0];
    }
};













另一個比較好理解作法是把數字丟到 vector 排序完在放回 list

但這樣很傷空間

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        vector<int> tmp;
        int i, nSize = lists.size();
        ListNode *dummy = new ListNode(), *current = dummy;

        for (i=0; i<nSize; i++) {
            current = lists[i];
            while (current) {
                tmp.push_back(current->val);
                current = current->next;
            }
        }

        sort(tmp.begin(), tmp.end());
        current = dummy;
        nSize = tmp.size();
        for (i=0; i<nSize; i++) {
            current->next = new ListNode(tmp[i]);
            current = current->next;
            current->next = NULL;
        }

        return dummy->next;
    }
};
