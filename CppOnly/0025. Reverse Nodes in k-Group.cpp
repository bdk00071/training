
/*
    題目要求一串 list 要分成 k Nodes 一組：有滿k 個要做 reverse，沒滿的不需要動
    所以我們要 dummy 跟 rDummy
    dummy後面是已經完全處理好的
    rDummy 後面是先看過了，但尚未確定要不要 reverse
    head 帶的是完全未處理的 nodes
    每次我們從 head 拿一個 node 出來，把 node 丟到 rDummy 尾端
    開始檢查 rDummy 後面有幾個 nodes，但這數量不包括 rDummy
    每次重新檢查太複雜，所以直接新增數字記錄
    如果已經有 k個那達成條件，把這串list反轉後放到 dummy 後面
    記得reset rDummy相關資料，不然一定會出錯
    到這確認每次 reverse 的 list 數量只會有 k 個
    且 reverse 之後就會放到已完成的 list 後面
    那不用 reverse怎麼辦？
    想想看什麼情況會有不需要 reverse 的 nodes：
    k = 5 且 head 帶了 13 個 nodes
    這樣跑完後 dummy 後面有 5 個一組 reversed 過的 nodes，總共 10 個
    另外 3 個在哪裡？
    答案是 rDummy 後面。因為我們只在滿 k 時處理 rDummy 後面的 nodes 
    只要在 head == NULL 時處理 rDummy 後面的 nodes 就可以把不需反轉的 nodes 放到需要的位置。
*/

class Solution {
public:
    ListNode* reverseList(ListNode *head) {
        ListNode *dummy = new ListNode, *tmp;
        while (head) {
            tmp = head;
            head = head->next;
            tmp->next = dummy->next;
            dummy->next = tmp;
        }

        return dummy->next;
    }

    ListNode* reverseKGroup(ListNode* head, int k) {
        int nCount = 0;
        ListNode *dummy = new ListNode, *current = dummy;
        ListNode *rDummy = new ListNode, *rCurrent = rDummy, *tmp;

        while (head) {
            rCurrent->next = head;
            rCurrent = rCurrent->next;
            head = head->next;
            rCurrent->next = NULL;
            nCount++;
            
            if (nCount >= k) {
                tmp = rDummy->next;
                current->next = reverseList(tmp);
                rDummy->next = NULL;
                while (current->next != NULL) current = current->next;
                nCount = 0;
                rCurrent = rDummy;
            }
        }

        if (rDummy->next != NULL) current->next = rDummy->next;
        
        return dummy->next;
    }
};
