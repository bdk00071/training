

//這題蠻有趣。
// 作法是依據有無相同移動 nums 並且讓 k 增加

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int n = nums.size(), k=1;
        for (int i = 1; i<n; i++) {
            if (nums[i] != nums[i-1]) nums[k++] = nums[i];
        }
        return k;
    }
};


// 淺顯易懂 Solution：檢查是否要把 i+nCount 的數字搬到 i 的位置上
// n - nCount 就是 return value

/*
    class Solution {
    public:
        int removeDuplicates(vector<int>& nums) {
            int nCount = 0, n=nums.size(), i;

            if (n==1) return 1;
            for (i = 1; i+nCount < n;) {
                if (nums[i+nCount] == nums[i+nCount-1]) {
                    nCount++;
                } else {
                    nums[i] = nums[i+nCount];
                    i++;
                }
            }

            return n-nCount;
        }
    };
*/