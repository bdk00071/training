
/*
    可以計算有多少個不等於 val
    在 nums[i] 不是 val 時把 nums[i] 搬去 idx j 的位置
    同時把　j++ 以存放下個不是 val 的數
    沒有 val 之後， j++ 也是 return 的數字
*/

class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int n = nums.size();
        int j = 0;
        for (int i=0;i<n;i++) {
            if (nums[i]==val) continue;
            nums[j++] = nums[i];
            cout << j << endl;
        }
        return j;
    }
};
