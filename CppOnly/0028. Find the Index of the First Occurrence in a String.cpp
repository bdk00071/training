/**
    用 lib 就可以直接解決
    想要用手動解決的話嘛
    for loop 跑 haystack
    index 的位置開始跟 needle 相同就直接 return 掉
    跑到結束都沒有就掰掰
 */


 class Solution {
public:
    int strStr(string haystack, string needle) {
        size_t found;
        found = haystack.find(needle);
        if (found != string::npos) return found;
        return -1;
    }
};