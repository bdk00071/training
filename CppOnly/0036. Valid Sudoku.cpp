// 依序檢查 row, column 跟 3x3




class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        for (int i = 0; i < 9; ++i) {
            if (!isValidRow(board, i))
                return false;
        }
        for (int j = 0; j < 9; ++j) {
            if (!isValidColumn(board, j))
                return false;
        }
        for (int i = 0; i < 9; i += 3) {
            for (int j = 0; j < 9; j += 3) {
                if (!isValidBlock(board, i, j))
                    return false;
            }
        }
        
        return true;
    }
    
private:
    bool isValidRow(const vector<vector<char>>& board, int row) {
        char num;
        unordered_set<char> seen;
        for (int j = 0; j < 9; ++j) {
            num = board[row][j];
            if (num != '.') {
                if (seen.count(num))
                    return false;
                seen.insert(num);
            }
        }
        return true;
    }
    
    bool isValidColumn(const vector<vector<char>>& board, int col) {
        char num;
        unordered_set<char> seen;
        for (int i = 0; i < 9; ++i) {
            num = board[i][col];
            if (num != '.') {
                if (seen.count(num)) return false;
                seen.insert(num);
            }
        }
        return true;
    }
    
    bool isValidBlock(const vector<vector<char>>& board, int startRow, int startCol) {
        char num;
        unordered_set<char> seen;
        for (int i = startRow; i < startRow + 3; ++i) {
            for (int j = startCol; j < startCol + 3; ++j) {
                num = board[i][j];
                if (num != '.') {
                    if (seen.count(num)) return false;
                    seen.insert(num);
                }
            }
        }
        return true;
    }
};
