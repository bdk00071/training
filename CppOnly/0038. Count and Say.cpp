/**

    嘖，已經到遞迴的做法了，速度好慢
    把 n=n-1 的做出來就可以做 n 的

 */


 class Solution {

    string say(string tmp) {
        string ans = "";
        int i = 0, nCount = 0;

        while (i <= tmp.length()) {
            if (i == 0) {
                nCount++;
            } else if (tmp[i] != tmp[i-1]) {
                ans = ans + to_string(nCount) + tmp[i-1];
                nCount = 1;
            } else {
                nCount++;
            }
            i++;
        }
        return ans;
    }

public:
    string countAndSay(int n) {
        string ans = "1";
        for (int i=1; i<n; i++) ans = say(ans);
        return ans;
    }
};