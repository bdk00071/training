
// backtrack
// 因為會有重複，所以在決定加入多少重複元素時要小心



class Solution {
public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        vector<int> tmp;
        vector<vector<int>> ans;
        
        generate(0, target, 0, candidates, tmp, ans);
        return ans;
    }

    void generate(int sum, int target, int idx, vector<int>& candidates, vector<int> tmp, vector<vector<int>>& ans) {
        int n=candidates.size();
        
        if (sum == target) {
            ans.push_back(tmp);
            return;
        } else if (sum > target || idx >= n) {
            return;
        }
        
        // 每次都只決定這次 num[idx] 要加幾個
        generate(sum, target, idx+1, candidates, tmp, ans);
        for (int i=1; sum<=target; i++) {
            sum += candidates[idx];
            tmp.push_back(candidates[idx]);
            generate(sum, target, idx+1, candidates, tmp, ans);
            if (target - sum < candidates[idx]) break;
        }

        return;
    }
};
