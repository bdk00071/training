/*
    40 跟 39 類似，都是組合元素求 sum == target
    但元素使用限制不同， 39 是給了元素後數量無限， 40 是只能用給的 elements
    數好重複元素有幾個， DFS 往下走的路就有幾條。
*/

class Solution {
public:
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        vector<int> tmp;
        vector<vector<int>> ans;
        
        sort(candidates.begin(), candidates.end());
        generate(0, target, 0, candidates, tmp, ans);
        return ans;
    }

    void generate(int sum, int target, int idx, vector<int>& candidates, vector<int> tmp, vector<vector<int>>& ans) {
        int n=candidates.size(), count = 0;
        
        if (sum == target) {
            ans.push_back(tmp);
            return;
        } else if (sum > target || idx >= n) {
            return;
        }

        // 先數有幾個 candidates[idx] 是一樣的可以用
        for (int i=0; i<n-idx; i++) {
            if (candidates[idx] == candidates[idx+i]) count++;
        }
        
        // 決定好要放進去幾個相同的數字後就跳到下個
        generate(sum, target, idx+count, candidates, tmp, ans);
        for (int i=1; sum<=target && i<=count; i++) {
            sum += candidates[idx];
            tmp.push_back(candidates[idx]);
            generate(sum, target, idx+count, candidates, tmp, ans);
            if (target - sum < candidates[idx]) break;
        }

        return;
    }
};
