
/*
    想計算 "每灘水有多少"，但困難是怎麼決定 "水停止累積"、"水開始累積"
    => 兩邊比較高才會積水
    => 可以從一個點往左右找，找到兩邊各自的最高點就是一灘水
    => 且兩個最高點中間的我都處理完

    找一灘水的體積用一格一格算比較好處理
    直接算每格會積多少水就好，每一格左右兩邊最高的都有超過它就會有水。
*/


class Solution {
public:
    int trap(vector<int>& height) {
        int n=height.size(), i, ans=0;
        vector<int> L = height, R = height;

        for (i=1; i<n; i++) L[i] = max(L[i], L[i-1]);
        for (i=n-2; i>=0; i--) R[i] = max(R[i+1], R[i]);
        for (int i=0; i<n; i++) {
            ans += max(min(L[i],R[i]) - height[i], 0);
        }

        return ans;
    }
};