/**
    就，乘法
    result 用來放乘完之後每一個位數是多少
    最高是 n1+n2，不會到這個長度
    兩個 for loop 是開始算乘法，有進位的要移走
    不然後面拚圖會有問題
    while 的地方是要把 result 開頭的 0 丟掉
    然後把非 0 放上去就好
 */


 class Solution {
public:
    string multiply(string num1, string num2) {
        if (num1 == "0" || num2 == "") return "0";

        int n1 = num1.length(), n2 = num2.length(), tmp, i, j;
        vector<int> result(n1+n2, 0);

        for (i=n1-1; i>=0; i--) {
            for (j=n2-1; j>=0; j--) {
                tmp = result[i+j+1] + (num1[i] - '0')*(num2[j] - '0');                
                result[i+j] += tmp/10;
                result[i+j+1] = tmp%10;
            }
        }

        i = 0;
        while (i < result.size() && result[i] == 0) i++;
        if (i == result.size()) return "0";

        string ans;
        for (; i < result.size(); i++) ans += to_string(result[i]);
        return ans;
    }
};