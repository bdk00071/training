
/**
    這題是處理 jump game
    1 是要回能不能跳到終點， 2 要計算跳幾次。
    用 Greedy 每次都選能跳最遠的可以解決。
 */

class Solution {
public:
    int jump(vector<int>& nums) {
        int idx=0, n = nums.size(), farest, tmpIdx, i, steps = 0;
        
        while (idx < n-1) {
            if (idx + nums[idx] >= n-1) return steps+1;
            tmpIdx = farest = 0;
            for (i=1; i<= nums[idx] && i<n; i++) {
                if (i + nums[idx+i] > farest) {
                    tmpIdx = i;
                    farest = i + nums[idx+i];
                }
            }

            idx+=tmpIdx;
            steps++;
            if (idx >= n-1) return steps;
            else if (farest == 0) return 0;
        }

        return steps;
    }
};
