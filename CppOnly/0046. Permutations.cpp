/*
    backtrack 題目
    用一個 vector 記著那些放過
    用 DFS 把全部都放一次就好
*/


class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) {
        vector<vector<int>> ans;
        vector<bool> used(6, false);
        vector<int> permutting;

        generate(ans, nums, used, permutting, 0, nums.size());
        return ans;
    }

    void generate(vector<vector<int>>& ans, vector<int>& nums, vector<bool>& used, vector<int> permutting, int lv, int n) {
        if (n == lv) {
            ans.push_back(permutting);
            return;
        }

        for (int i=0; i<n; i++) {
            if (used[i]) continue;
            permutting.push_back(nums[i]);
            used[i] = true;
            generate(ans, nums, used, permutting, lv+1, n);
            permutting.pop_back();
            used[i] = false;
        }

        return;
    }
};
