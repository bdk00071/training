
/*
    跟 0046 不同的地方是會有重複數字
    觀察可以發現 "前個數字相同而且沒有挑選，現在這數字就不該挑" 就不會重複
    原因是能在這個位置開始挑出來的排序
    但前一個有同樣數字卻又沒挑，表示我可以從前一個數字挑出一樣的排序
    =>  不要拿
*/



class Solution {
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        int n = nums.size();
        vector<bool> used(nums.size(), 0);
        vector<vector<int>> ans;
        vector<int> tmp;

        sort(nums.begin(), nums.end());
        generate(nums, ans, used, n, 0, tmp);
        return ans;
    }

    void generate(vector<int>& nums, vector<vector<int>>& ans, vector<bool>& used, int lv, int path, vector<int> tmp) {
        if (lv == path) {
            ans.push_back(tmp);
            return;
        }

        for (int i=0; i<lv; i++) {
            if (i > 0 && nums[i] == nums[i - 1] && used[i - 1] == false) continue;
            
            if (used[i] == false) {
                used[i] = true;
                tmp.push_back(nums[i]);
                generate(nums, ans, used, lv, path+1, tmp);
                used[i] = false;
                tmp.pop_back();
            }
        }
        return;
    }
};
