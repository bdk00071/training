/*
    "ab", "ba" 怎麼分類到一起
    sort() 可以做好這事。
*/

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map <string, vector<string>> mp;
        vector<vector<string>> ans;
        string sorted;

        for (auto s : strs) {
            sorted = s;
            sort(sorted.begin(), sorted.end());
            mp[sorted].push_back(s);
        }

        for (auto s : mp) ans.push_back(s.second);
        return ans;
    }
};
