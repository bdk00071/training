

/**
    這題可以用 DP 解並且直接在 nums 裡面算。
    nums[i] 存 index i 結尾 subarray 的最大 sum
    nums[i+1] 的值算法是看 nums[i] 大於0 做決定
    計算 nums[0] ~ nums[n-1] 過程記下 max 即可
 */


class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int val = nums[0], i=0, n = nums.size();
        // DP：nums[i] means the ans of the array with index 0 ~ i.
        for (i=1; i<n; i++) {
            if (nums[i-1] > 0) nums[i] += nums[i-1];
            val = max(val, nums[i]);
        }
        return val;
    }
};
