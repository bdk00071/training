
// 用 Greedy 可以解。每次都挑能跳最遠的就能處理掉，發現最遠的是 0 就要結束。


class Solution {
public:
    bool canJump(vector<int>& nums) {
        int idx=0, n = nums.size(), farest, tmpIdx, i;
        while (idx < n-1) {
            tmpIdx = farest = 0;
            for (i=1; i<= nums[idx] && i<n; i++) {
                if (i + nums[idx+i] > farest) {
                    tmpIdx = i;
                    farest = i + nums[idx+i];
                }
            }

            idx+=tmpIdx;
            if (idx >= n-1 || idx + nums[idx] >= n-1) return true;
            else if (farest == 0) return false;
        }

        return idx >= n-1;
    }
};
