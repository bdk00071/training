
/*
    要在一個區間群裡面多塞一個區間然後合併有重複的。

    [a,b], [c,d]
    以上兩區間怎樣檢查須要合併？（不考慮 b>=a 這種 case）
    不知道哪個區間比較小的情況是 b>c 或者 d>a
    但 sort 可以幫我們處理，條件剩下 b>c
    合併後請不要直接跳去處理下一個
    所以加上一個 i-- 檢查合併完的區間和下一個是否要合併
*/

class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        vector<vector<int>> ans;

        sort(intervals.begin(), intervals.end());
        for (int i=1; i<intervals.size(); i++) {
            if (intervals[i-1][1] >= intervals[i][0]) {
                intervals[i-1][1] = max(intervals[i][1], intervals[i-1][1]);
                intervals.erase(intervals.begin() + i);
                i--;
            }
        }

        return intervals;
    }
};



/*
    底下這比較單純，一個 for loop 處理 intervals
    iStart, iEnd 記準備放 ans 的 interval
    如果都是 -1 那就依然是空
    現在的 interval 跟 iEnd 有交錯就重新決定頭尾
    沒交錯就放進 ans & 重設 iStart、iEnd
*/

class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        int i, n = intervals.size(), iStart = -1, iEnd = -1;
        vector<vector<int>> ans;

        sort(intervals.begin(), intervals.end());
        for (i=0; i<n; i++) {
            if (iStart == -1 && iEnd == -1) {
                iStart = intervals[i][0];
                iEnd   = intervals[i][1];
            } else if (iEnd >= intervals[i][0]) {
                iStart = min(iStart, intervals[i][0]);
                iEnd   = max(iEnd, intervals[i][1]);
            } else {
                ans.push_back({iStart, iEnd});
                iStart = intervals[i][0];
                iEnd   = intervals[i][1];
            }
        }

        if (iStart != -1 && iEnd != -1) ans.push_back({iStart, iEnd});
        return ans;
    }
};






