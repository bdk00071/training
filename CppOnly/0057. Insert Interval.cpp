/*
    跟 0056 有點像，但這題的 interval 要自己塞進去
*/


class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
        intervals.push_back(newInterval);
        sort(intervals.begin(), intervals.end());
        int i=0, n = intervals.size();
        while (i != n-1) {
            if (intervals[i][1] >= intervals[i+1][0]) {
                // merge !!
                intervals[i][1] = max(intervals[i][1], intervals[i+1][1]);
                intervals.erase(intervals.begin()+i+1);
                n--;
            } else {
                i++;
            }
        }

        return intervals;
    }
};
