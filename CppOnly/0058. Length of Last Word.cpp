/**
    反轉整個字串，清掉最前面的空格
    找第一個空格後就有答案了
 */


 class Solution {
public:
    int lengthOfLastWord(string s) {
        int ans;
        std::reverse(s.begin(), s.end());

        int idx = s.find_first_not_of(" ");
        while (s[0] == ' ') s = s.substr(1);
        
        ans = s.find_first_of(" ");
        
        if (ans == -1) return s.length();
        return ans;
    }
};