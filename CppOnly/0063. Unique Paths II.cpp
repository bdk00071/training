
// DP 走路題，但這次多了石頭擋著不能走需要注意。


class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        if (obstacleGrid[0][0] == 1) return 0;
        vector<vector<int>> table = obstacleGrid;
        int m = obstacleGrid.size(), n = obstacleGrid[0].size(), i, j;
        
        for (i=0; i<m; i++) {
            table[i].assign(n, 0);
        }
        table[0][0] = 1;

        for (i=0; i<n; i++) {       // for row
            for (j=0; j<m; j++) {   // for column
                if (obstacleGrid[j][i] == 1) continue;

                if (i != 0 && obstacleGrid[j][i-1] != 1) {
                    table[j][i] += table[j][i-1];
                }

                if (j != 0 && obstacleGrid[j-1][i] != 1) {
                    table[j][i] += table[j-1][i];
                }
            }
        }

        return table[m-1][n-1];
    }
};
