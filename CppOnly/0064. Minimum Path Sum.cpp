
// DP 解法，因為求最低 value 所以每次走都找最低值方向。



class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        int n = grid.size(), m = grid[0].size();
        for (int i=0; i<n; i++) {
            for (int j=0; j<m; j++) {
                if (i+j==0) continue;
                else if (j==0) grid[i][j] += grid[i-1][j];
                else if (i==0) grid[i][j] += grid[i][j-1];
                else {
                    grid[i][j] += min(grid[i-1][j], grid[i][j-1]);
                }
            }
        }

        return grid[n-1][m-1];
    }
};
