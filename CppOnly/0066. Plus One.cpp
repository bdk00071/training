
/*
    給個 vector 表示數字， +1 後 return。
    想考操作 vector 吧，蠻多這種r加減乘除題
    這題只 +1 所以沒什麼難度。
*/

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        int n = digits.size(), carry=0;

        digits[n-1]++;
        for (int i=n-1; i>=0; i--) {
            digits[i] += carry;
            carry = 0;
            if (digits[i]>=10) {
                digits[i] %=10;
                carry = 1;
            }
        }
        if (carry==1) digits.insert(digits.begin(), 1);
        return digits;
    }
};
