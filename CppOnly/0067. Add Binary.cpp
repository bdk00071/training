// As title, 不多說。



class Solution {
public:
    string addBinary(string a, string b) {
        int A = a.length()-1, B = b.length()-1, two = 0, tmp;
        string ans = "";
        while (A >= 0 || B >= 0) {
            if (A >= 0 && B >= 0) {
                tmp = a[A] - '0' + b[B] - '0' + two;
                two = 0;
                if (tmp > 1) {
                    two = tmp/2;
                    tmp %= 2;
                }
                ans = to_string(tmp) + ans;
            } else if (A >= 0) {
                tmp = a[A] - '0' + two;
                two = 0;
                if (tmp > 1) {
                    two = tmp/2;
                    tmp %= 2;
                }
                ans = to_string(tmp) + ans;
            } else if (B >= 0) {
                tmp = b[B] - '0' + two;
                two = 0;
                if (tmp > 1) {
                    two = tmp/2;
                    tmp %= 2;
                }
                ans = to_string(tmp) + ans;
            }
            A--;
            B--;
        }
        if (two) {
            ans = "1" + ans;
        }
        return ans;
    }
};
