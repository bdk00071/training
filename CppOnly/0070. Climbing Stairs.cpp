
/*
    經典 DP 教學，跟 fib 是一樣的東西
    DP[i] 是爬到第 i 層有幾種方法，這格該怎麼填？
    因為可以走一或二步，所以能到 i 層的只有 DP[i-1] 和 DP[i-2]
*/


class Solution {
public:
    int climbStairs(int n) {
        int ans[n+1];
        ans[0] = ans[1] = 1;
        for (int i=2; i<=n; i++) ans[i] = ans[i-1]+ans[i-2];
        return ans[n];
    }
};
