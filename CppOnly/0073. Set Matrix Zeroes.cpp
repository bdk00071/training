
/**
    初始矩陣有出現 0 的 row, column 都改成 0
    先找出一個 0 ，這樣就能把要改 0 的位置記在這 row\column
    把該清掉的全部清掉後回頭處理這 row\column
 */



class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        int m = matrix.size(), n = matrix[0].size();
        int nRow = -1, nColumn = -1;
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    nRow = i;
                    nColumn = j;
                    break;
                }
            }
            if (nRow != -1) {
                break;
            }
        }
        
        if (nRow == -1) return;
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    matrix[i][nColumn] = 0;
                    matrix[nRow][j] = 0;
                }
            }
        }

        for (int i = 0; i < m; i++) {
            if (i == nRow) continue;
            for (int j = 0; j < n; j++) {
                if (j == nColumn) continue;
                if (matrix[i][nColumn] == 0 || matrix[nRow][j] == 0) matrix[i][j] = 0;
            }
        }
        for (int j = 0; j < n; j++) matrix[nRow][j] = 0;
        for (int i = 0; i < m; i++) matrix[i][nColumn] = 0;
    }
};
