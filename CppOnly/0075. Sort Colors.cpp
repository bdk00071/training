
/*
    不能用 sort()
    數 1,2,3 的數量後直接改數字
*/


class Solution {
public:
    void sortColors(vector<int>& nums) {
        int one = 0, two = 0, zero = 0, n=nums.size(), i;

        for (i=0; i<n; i++) {
            if (nums[i] == 0) zero++;
            else if (nums[i] == 1) one++;
            else if (nums[i] == 2) two++;
        }

        i=0;
        while (i<zero) {
            nums[i] = 0;
            i++;
        }
        while (i<zero+one) {
            nums[i] = 1;
            i++;
        }
        while (i<zero+one+two) {
            nums[i] = 2;
            i++;
        }
    }
};