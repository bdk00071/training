/*
    n 取 k 有幾種取法， return 集合。
    沒有重複難度就不高，一個一個挑到有 k 個就可以 return
*/

class Solution {
public:
    vector<vector<int>> combine(int n, int k) {
        vector<vector<int>> ans;
        walk(n, k, 0, ans, {});

        return ans;
    }

    void walk(int n, int k, int idx, vector<vector<int>>& ans, vector<int> choosen) {
        if (choosen.size() == k) {
            ans.push_back(choosen);
            return;
        }

        for (int i=idx+1; i<=n; i++) {
            choosen.push_back(i);
            walk(n, k, i, ans, choosen);
            choosen.pop_back();
        }

        return;
    }
};
