
/*
    idx 從 0 ~ nums.size()-1 走一輪就做好一個 ans 的 element
    而 Generate() 裡面需要處理的是 "我要不要放 nums[idx]"
    答案會是都要
    而因為 tmp 沒有用 reference 所以先做哪個無所謂

    這個 solution 比前一個簡單許多
*/



class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> ans;
        
        Generate(0, nums, {}, ans);
        return ans;
    }

    void Generate(int idx, vector<int>& nums, vector<int> tmp, vector<vector<int>>& ans) {
        if (idx >= nums.size()) {
            ans.push_back(tmp);
            return;
        }

        Generate(idx+1, nums, tmp, ans);
        tmp.push_back(nums[idx]);
        Generate(idx+1, nums, tmp, ans);
        return;
    }
};
