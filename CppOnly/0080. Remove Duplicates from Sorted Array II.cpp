/**

    初版是完全沒重複
    這題要求可以重複兩個
    解題概念用個 for loop 整理 vector
    檢查要不要把 index i 搬去 index k
    因為 nums[i-1], nums[i-2] 是還沒擺好的 elements
    條件要看已經處理好的 nums[k-1], nums[k-2] 才對
    
 */



 class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int n = nums.size(), k = 2;

        if (n<2) return n;
        for (int i=2; i<n; i++) {
            // 要不要把 num[i] 寫到 index k
            if (nums[i] != nums[k-1] || nums[i] != nums[k-2]) {
                nums[k++] = nums[i];
            }
        }
        return k;
    }
};