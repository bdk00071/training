
/*
    從空著的地方開始擺，如果是 nums1 全部擺在 nums2 後面
    出現的狀況也是把 nums1 一個一個搬去 vector 後端然後把 nums2 放到前端
    順序反過來也不會影響我們擺放，兩種 edge cases 的空間都一定夠。
*/



class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        int k = nums1.size()-1;
        while (m-1 >=0 || n-1 >=0) {
            if (m-1 >=0 && n-1 >=0) {
                if (nums1[m-1] > nums2[n-1]) {
                    nums1[m+n-1] = nums1[m-1];
                    m--;
                } else {
                    nums1[m+n-1] = nums2[n-1];
                    n--;
                }
            } else if (n-1 >= 0) {
                nums1[n-1] = nums2[n-1];
                n--;
            } else if (m-1 >=0) {
                m--;
            }
        }

        return;
    }
};
