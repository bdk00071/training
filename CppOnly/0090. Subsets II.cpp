
// 這次的 power set 有重複元素，所以修改成一次看完整個 set 要加入那些元素才放入 ans 。



class Solution {
public:
    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        vector<vector<int>> ans;
        sort(nums.begin(), nums.end());

        generate(0, nums.size(), nums, {}, ans);
        return ans;
    }

    void generate(int idx, int size, vector<int>& nums, vector<int> tmp, vector<vector<int>>& ans) {
        if (idx == size) {
            // idx 要已經看完一輪才能放
            ans.push_back(tmp);
            return;
        }

        int cnt = 0, i;
        for (i=idx; i<size; i++) {
            // count the numbers of nums[idx]
            if (nums[i] == nums[idx]) cnt ++;
        }

        for (i=0; i<=cnt; i++) {
            // 一次決定 nums[idx] 要放多少個，所以用 for loop 把放置數量都跑一輪
            if (i != 0) tmp.push_back(nums[idx]);
            // 因為都決定完了， index 直接跳去下個數字
            generate(idx+cnt, size, nums, tmp, ans);
        }
    }
};
