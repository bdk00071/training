/*
    把一個 string 拆成 IP 格式。問所有組合。
    一樣用 backtrack 去找。
*/




class Solution {
public:
    vector<string> restoreIpAddresses(string s) {
        vector<string> ans;
        vector<string> path;
        walk(s, 0, path, ans);
        return ans;
    }
    
private:
    void walk(const string& s, int start, vector<string>& path, vector<string>& result) {
        if (path.size() == 4 && start == s.length()) {
            result.push_back(path[0] + "." + path[1] + "." + path[2] + "." + path[3]);
            return;
        }
        
        for (int i = 1; i <= 3; ++i) {
            if (start + i > s.length() || !isValidPart(s.substr(start, i))) continue;
            
            string part = s.substr(start, i);
            path.push_back(part);
            walk(s, start + i, path, result);
            path.pop_back();
        }
    }
    
    bool isValidPart(const string& part) {
        if (part.empty() || (part[0] == '0' && part.length() > 1) || stoi(part) > 255) {
            return false;
        }
        return true;
    }
};
