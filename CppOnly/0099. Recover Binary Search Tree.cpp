/**
    BST 上交換兩個 nodes
    修回來。

    BST 用 inorder 是 increasing
    換過兩個 nodes 表示會有 "pre > current"
    檢查後可以知道如果是交換沒有連在一起的 nodes
    第一次遇到的要拿 pre，第二次要拿 current
    mark 是提醒兩個連在一起的話會是這樣拿
 */


 class Solution {
    TreeNode *pre, *first, *second;

    void inorder(TreeNode* root) {
        if (!root) return;

        inorder(root->left);
        if (pre != NULL && pre->val > root->val) {
            if (first == NULL) first = pre;
            second = root; // attention
        }
        pre = root;
        inorder(root->right);
    }
public:
    void recoverTree(TreeNode* root) {
        pre = first = second = NULL;
        inorder(root);
        swap(first->val, second->val);
    }
};