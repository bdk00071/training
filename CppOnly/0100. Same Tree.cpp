
// 要比較兩個 tree 是否一樣。把每個 node 都當作 root 看 left、right 是否有問題，全都沒問題就相同了。


class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        if (p == NULL && q != NULL) return false;
        if (q == NULL && p != NULL) return false;
        if (q == NULL && p == NULL) return true;
        if (p->val != q->val) return false;
        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
    }
};
