
/*
    要有一個走法是在左右兩邊的位置是相對的，
    一邊往左時另一邊要往右，要同時為空或有資料，val 要相等。
    可以判斷失敗或這邊沒得走了的條件都檢查完後就是走 children 或 return
*/

class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        if (root == NULL) return true;

        return check(root->left, root->right);
    }

    bool check(TreeNode* l, TreeNode* r) {
        if (l==NULL && r==NULL) return true;
        if (l==NULL && r != NULL) return false;
        if (l!=NULL && r == NULL) return false;
        if (l->val != r->val) return false;

        return check(l->left, r->right) && check(l->right, r->left);
    }
};
