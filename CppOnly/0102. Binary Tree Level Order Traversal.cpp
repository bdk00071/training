/*
    level order 就是要一層層走過去
    把 root 塞在 vector
    裡面 nodes 的 children 都塞進另一個容器
*/

class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        int i, n;
        vector<vector<int>> ans;
        vector<TreeNode*> tree, children;

        if (root != NULL) tree.push_back(root);
        while (tree.size()!=0) {
            ans.push_back({});
            for (i=0; i<tree.size(); i++) {
                if (tree[i]->left != NULL)  children.push_back(tree[i]->left);
                if (tree[i]->right != NULL) children.push_back(tree[i]->right);
                ans[ans.size()-1].push_back(tree[i]->val);
            }
            tree = children;
            children = {};
        }

        return ans;
    }
};
