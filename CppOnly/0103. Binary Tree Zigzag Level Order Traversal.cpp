
/*
    如果要蛇行排列，那們多加一個 true/false 變化的參數，
    在結束這 level 前幫我們檢查是否需要 reverse 就可以完成
*/



class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        int i, n;
        bool zigzag = true;
        vector<vector<int>> ans;
        vector<TreeNode*> tree, children;

        if (root != NULL) tree.push_back(root);
        while (tree.size()!=0) {
            ans.push_back({});
            zigzag = !zigzag;
            for (i=0; i<tree.size(); i++) {
                if (tree[i]->left != NULL)  children.push_back(tree[i]->left);
                if (tree[i]->right != NULL) children.push_back(tree[i]->right);
                ans[ans.size()-1].push_back(tree[i]->val);
            }
            if (zigzag) reverse(ans[ans.size()-1].begin(), ans[ans.size()-1].end());
            tree = children;
            children = {};
        }

        return ans;
    }
};
