/*
    寫完才意識到這個用遞迴就好
    不需要多用 parameter 傳
*/

class Solution {
public:
    int maxDepth(TreeNode* root) {
        if (root==NULL) return 0;
        return help(root, 0);
    }

    int help(TreeNode* root, int lv) {
        if (root->left == NULL && root->right == NULL) return lv+1;

        if (root->left  == NULL) return help(root->right, lv) +1;
        if (root->right == NULL) return help(root->left,  lv) +1;

        return max(help(root->left,  lv), help(root->right, lv))+1;
    }
};