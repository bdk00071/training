
/**
    概念是 preorder[0] 一定是 tree 的 root
    保證 unique values 的關係，可以在 inorder 分出 [left] preorder[0] [right]
    這些資料夠我們回 preorder 找 left, right tree 的 preorder
    然後用 recursive 做 left/right tree

    讓題目簡單點：
                    處理 nodes 數量算式有點複雜
                    多個 nCount 就是很好算的
    所以 root->left 的 preorder ： pStart+1 開始 nCount 個 nodes
                       inorder  ： iStart 到 idx-1
         root->right 的 preorder ： left tree 後面開始，到 pEnd
                        inorder ： idx+1 開始到 iEnd
 */


class Solution {
    TreeNode* build(vector<int> preorder, int pStart, int pEnd,
                        vector<int> inorder, int iStart, int iEnd) {
        if (iStart > iEnd || pStart > pEnd) return NULL;

        int idx, nCount = 0;
        TreeNode *root = new TreeNode(preorder[pStart]);

        for (idx = iStart; idx<=iEnd; idx++) {
            if (inorder[idx] == root->val) break;
            nCount++;
        }
        root->left  = build(preorder, pStart+1, pStart+nCount,
                inorder, iStart, idx-1);
        root->right = build(preorder, pStart+nCount+1, pEnd,
                inorder, idx+1, pEnd);
        return root;
    }
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        return build(preorder, 0, preorder.size()-1, inorder, 0, inorder.size()-1);
    }
};