/**
    概念跟 105 一樣
    postorder 最後一個 node 可以讓 inorder 分出 left, right
    用這樣做遞迴就能完成
    找 inorder 的位置一起數 left 有多少 node 會讓事情比較好做
 */
class Solution {
    TreeNode* build(vector<int> postorder, int pStart, int pEnd,
                        vector<int> inorder, int iStart, int iEnd) {
        if (pStart > pEnd || iStart > iEnd) return NULL;
        
        TreeNode *root = new TreeNode(postorder[pEnd]);
        int idx, nCount = 0;
        for (idx=iStart; idx<=iEnd; idx++) {
            if (inorder[idx] == root->val) break;
            nCount++; // nodes of left tree
        }
        root->left = build(postorder, pStart, pStart+nCount-1, inorder, iStart, idx-1);
        root->right= build(postorder, pStart+nCount, pEnd-1, inorder, idx+1, iEnd);
        return root;
    }
public:
    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        return build(postorder, 0, postorder.size()-1, inorder, 0, inorder.size()-1);
    }
};