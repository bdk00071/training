
/**
    level order 要求從底下排到上面。
    用 102 的 codes 再 return 前對 ans 做 reverse() 就可以通過。
    有另外的想法但還是用 reverse 最簡潔。
*/




class Solution {
public:
    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        int i, n;
        vector<vector<int>> ans;
        vector<TreeNode*> tree, children;

        if (root != NULL) tree.push_back(root);
        while (tree.size()!=0) {
            ans.push_back({});
            for (i=0; i<tree.size(); i++) {
                if (tree[i]->left != NULL)  children.push_back(tree[i]->left);
                if (tree[i]->right != NULL) children.push_back(tree[i]->right);
                ans[ans.size()-1].push_back(tree[i]->val);
            }
            tree = children;
            children = {};
        }

        reverse(ans.begin(), ans.end());
        return ans;
    }
};
