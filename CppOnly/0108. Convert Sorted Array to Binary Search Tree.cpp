/**
    用 recursive 就很簡單
    subvector 是半開
    每次都取 mid 做成 root
    vector 左右都分出去繼續做
    如果空了沒東西就結束
 */


 class Solution {
public:
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        if (nums.size()==0) return NULL;
        int mid = nums.size()/2;
        TreeNode* root = new TreeNode(nums[mid]);
        vector<int> tmp;
        
        tmp = {nums.begin(), nums.begin()+mid};
        root->left  = sortedArrayToBST(tmp);
        tmp = {nums.begin()+mid+1, nums.end()};
        root->right = sortedArrayToBST(tmp);
        return root;
    }
};