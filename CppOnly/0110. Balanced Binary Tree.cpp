/**
    Balanced Tree： left\right tree 的深度相差不超過 1
    這樣的 Tree 會把 Nodes 放的比較平均
    檢查方法是每個 nodes 走一輪，直接追 left\right 的 depth
    土法煉鋼可以做完都會是 easy
 */
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        if (root == NULL) return true;

        if (abs(maxDepth(root->left) - maxDepth(root->right)) > 1) return false;

        if (isBalanced(root->right) == false) return false;
        if (isBalanced(root->left)  == false) return false;
        
        return true;
    }
    int maxDepth(TreeNode* root) {
        if (root == 0) return 0;
        else if (root->left==NULL && root->right==NULL) return 1;

        return max(maxDepth(root->left), maxDepth(root->right)) +1;
    }
};