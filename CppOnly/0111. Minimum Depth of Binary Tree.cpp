

// 這題跟找最深的相同方式，往左右 subtree 找最淺的 leaf 位置


class Solution {
public:
    int minDepth(TreeNode* root) {
        if (root == NULL) return 0;

        if (root->left == NULL && root->right == NULL) return 1;

        int left = minDepth(root->left), right = minDepth(root->right);
        if (left == 0) left = INT_MAX;
        if (right == 0) right = INT_MAX;
        return min(left, right)+1;
    }
};
