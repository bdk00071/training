
// 有路就走走看，到沒得走了就檢查一下是否與 targetSum 相符



class Solution {
public:
    bool hasPathSum(TreeNode* root, int targetSum) {
        if (root == NULL) return false;
        targetSum -= root->val;
        if (root->left == NULL && root->right == NULL && targetSum == 0) return true;

        if (true == hasPathSum(root->left, targetSum))  return true;
        if (true == hasPathSum(root->right, targetSum)) return true;

        return false;
    }
};
