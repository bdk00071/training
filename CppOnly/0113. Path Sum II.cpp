
/*
    與 112 類似，但這次要記下走的路徑。
    帶 tmp 記現在走的路，合條件就放到 ans
*/


class Solution {
    void walk(TreeNode* root, int targetSum, vector<vector<int>>& ans, vector<int> tmp, int sum) {
        sum += root->val;
        tmp.push_back(root->val);

        if (root->left == NULL && root->right == NULL) {
            if (targetSum == sum) ans.push_back(tmp);
            return;
        } else {
            if (root->left) walk(root->left,  targetSum, ans, tmp, sum);
            if (root->right) walk(root->right, targetSum, ans, tmp, sum);
        }

        return;
    }
public:
    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
        vector<vector<int>> ans;
        vector<int> tmp;

        if (root) walk(root, targetSum, ans, tmp, 0);

        return ans;
    }
};
