
/*
    tree level order
    將 tree 的 structure 改造了一下，有 'next' ptr
    將 next 指向同 level 右邊的 Node && 最右邊的指 NULL
    level order 走一圈就可以把 ptr 設定好。
*/


class Solution {
public:
    Node* connect(Node* root) {
        int i, n;
        vector<Node*> tree, children;

        if (root != NULL) tree.push_back(root);
        while (tree.size()!=0) {
            n = tree.size();
            for (i=0; i<n; i++) {
                if (tree[i]->left != NULL)  children.push_back(tree[i]->left);
                if (tree[i]->right != NULL) children.push_back(tree[i]->right);
                if (i != n-1) tree[i]->next = tree[i+1];
            }
            tree = children;
            children = {};
        }

        return root;
    }
};
