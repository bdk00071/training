
/*
    跟 116 很像
    116 是 perfect binary tree
    我是 level order 走完拿到答案
    所以相同的 code 可以用在這一題
 */


class Solution {
public:
    Node* connect(Node* root) {
        int i, n;
        vector<Node*> tree, children;

        if (root != NULL) tree.push_back(root);
        while (tree.size()!=0) {
            n = tree.size();
            for (i=0; i<n; i++) {
                if (tree[i]->left != NULL)  children.push_back(tree[i]->left);
                if (tree[i]->right != NULL) children.push_back(tree[i]->right);
                if (i != n-1) tree[i]->next = tree[i+1];
            }
            tree = children;
            children = {};
        }

        return root;
    }
};