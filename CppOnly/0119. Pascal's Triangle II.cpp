

// 類似118，不多說


class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<vector<int>> ans;
        vector<int> tmp;

        for (int i=0; i<=rowIndex; i++) {
            tmp = {};
            for (int j=0; j<=i; j++) {
                if (i<=1 || j==0 || j==i) {
                    tmp.push_back(1);
                } else {
                    tmp.push_back(ans[i-1][j-1]+ans[i-1][j]);
                }
            }
            ans.push_back(tmp);
        }
        return tmp;
    }
};
