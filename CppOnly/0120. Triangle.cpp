
// 類似走 tree 的題目，只是環境換成 vector。


class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) {
        for (int i=triangle.size()-2; i>=0; i--) {
            for (int j=0; j<=i; j++) {
                triangle[i][j] += min(triangle[i+1][j], triangle[i+1][j+1]);
            }
        }
        return triangle[0][0];
    }
};
