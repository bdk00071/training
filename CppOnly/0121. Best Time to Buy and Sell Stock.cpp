

// 找跟左邊最高點的最大差距。


class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int ans = 0, n=prices.size();
        for (int i=n-2; i>=0; i--) {
            ans = max(ans, prices[i+1] - prices[i]);
            prices[i] = max(prices[i+1], prices[i]);
        }

        return ans;
    }
};
