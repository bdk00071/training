
/*
    一次只能買一個股票，求最多能賺多少
    有上漲就買 & 賣
*/



class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int n=prices.size(), total=0;
        for (int i=0; i<n-1; i++) {
            if (prices[i+1]>prices[i]) 
                total += prices[i+1]-prices[i];
        }

        return total;
    }
};
