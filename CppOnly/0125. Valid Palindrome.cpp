/**
    用 std function 把 string 改成要求的樣子就可以簡單比對
 */


 class Solution {
public:
    bool isPalindrome(string s) {
        int n = s.length(), i, j;
        if (n == 1) return true;
        
        string s2 = "";
        for (i = 0; i < n; i++) if (isalnum(s[i])) s2 +=s[i];
        
        transform(s2.begin(), s2.end(), s2.begin(), ::toupper);

        i = 0;
        j = s2.length()-1;
        
        while (i<j) {
            if (s2[i] != s2[j]) return false;
            i++;
            j--;
        }
        return true;
    }
};