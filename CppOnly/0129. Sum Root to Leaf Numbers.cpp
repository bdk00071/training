
/*
    有路就往下走，left & right 都各自 call function 分散出去
    走到 leaf 時把留下的 tmp 加到 ref ans
*/




class Solution {
public:
    int sumNumbers(TreeNode* root) {
        int ans=0;
        if (root != NULL) walk(root, 0, ans);

        return ans;
    }

    void walk(TreeNode* root, int tmp, int& ans) {
        tmp = tmp*10 + root->val;

        if (root->left == NULL && root->right == NULL) {
            ans += tmp;
            return;
        }
        
        if (root->left  != NULL) walk(root->left,  tmp, ans);
        if (root->right != NULL) walk(root->right, tmp, ans);

        return;
    }
};
