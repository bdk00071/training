
/*
    這题有 bitwise 的解，或者用 map / hash 整理後找只有一個
    多加一個暴力解法， sort 完 for loop 看自己跟左右鄰居是否相同就可以找到
*/

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        map<int,int> m;
        for (int i = 0; i < nums.size(); i++) {
            m[nums[i]]++;
        }
        for (auto i:m) {
            if (i.second == 1) {
                return i.first;
            }
        }
        return -1;
    }
};



class Solution {
public:
    int singleNumber(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        int n=nums.size();

        if (n==1) return nums[0];

        for (int i=0; i<n; i++) {
            if (i==0 && nums[i] != nums[i+1]) {
                return nums[i];
            } else if (i==n-1 && nums[i] != nums[i-1]) {
                return nums[i];
            } else if (nums[i] != nums[i+1] && nums[i] != nums[i-1]) {
                return nums[i];
            }
        }

        return 0;
    }
};