/**
 *  整理好每個空格就不困難
 *  每次都先把開頭的空格掃乾淨
 *  然後把連續的非空格 char 放到 ans
 *  重複前兩個動作就好
 */


 class Solution {
public:
    string reverseWords(string s) {
        string ans = "", tmp;
        int idx;

        while (s.length() != 0) {
            // 先清掉前面空格
            idx = s.find_first_not_of(" ");
            if (idx < 0) break;
            else s = s.substr(idx);

            // 拿第一個字，現在 s 開頭不是空格了
            idx = s.find_first_of(" ");

            if (idx == -1) {
                if (ans == "") ans = s;
                else ans = s + " " + ans;
                s = "";
            } else {
                if (ans == "") ans = s.substr(0,idx);
                else ans = s.substr(0,idx) + " " + ans;
                s = s.substr(idx);
            }
        }

        return ans;
    }
};