/**

    題目描述已經直接給做法
    sort 後記下最大的 gap即可

 */


 class Solution {
public:
    int maximumGap(vector<int>& nums) {
        int ans = 0;

        sort(nums.begin() , nums.end());
        for (int i = 0; i < nums.size()-1 ;i++ ) {
            if (ans < nums[i+1] - nums[i]) {
                ans = nums[i+1] - nums[i];
            }
        }
        return ans;
    }
};