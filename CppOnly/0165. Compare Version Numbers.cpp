/**
    分兩個動作
    剛開始先把 2 versions 拆成 vector<int> 方便比較
    compare 用 index 比較就好，如果兩個都還存在就好處理
    有一個不存在就得考慮兩種情況
    實際情形並不難，只有轉成 vector<int> 要想久一點
 */



 class Solution {
public:
    vector<int> transfer(string version) {
        size_t v;
        string current;
        vector<int> ans;

        do {
            v = version.find(".");
            if (v != std::string::npos) {
                current = version.substr(0, v);
                version = version.substr(v+1);
                ans.push_back(stoi(current));
            } else {
                ans.push_back(stoi(version));
                version = "";
            }

        } while (version != "");
        return ans;
    }

    int compareVersion(string version1, string version2) {
        vector<int> v1 = transfer(version1), v2 = transfer(version2);
        int n = v1.size(), m = v2.size();

        for (int i=0; i<n || i<m; i++) {
            if (i<n && i<m) {
                if (v1[i] > v2[i]) return 1;
                else if (v1[i] < v2[i]) return -1;
            } else if (i>=n && i<m && v2[i] > 0) {
                return -1;
            } else if (i<n && i>=m && v1[i] > 0) {
                return 1;
            }
        }

        return 0;
    }
};