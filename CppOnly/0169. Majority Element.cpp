/**
    array 中有一個必存在而且數量超過一半的數字
    => 就把 elements 排序過後 array 中間一定就是這數字
 */


 class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int n = nums.size();
        
        sort(nums.begin(), nums.end());
        return nums[n/2];
    }
};