/**
    要把數字依照要求排序：誰放左邊才能湊出比較大的數字
    然後就只是把數字轉字串
 */



class Solution {
public:

    static bool cmp(int a, int b) {
        if (a==0) return false;
        if (b==0) return true;

        string ab = std::to_string(a) + std::to_string(b);
        string ba = std::to_string(b) + std::to_string(a);
        
        if (ab==ba) return true;
        for (int i=0; i<ab.length(); i++) {
            if (ab[i] - '0' > ba[i] - '0') return true;
            else if (ab[i] - '0' < ba[i] - '0') return false;
        }
        return true;
    }

    string largestNumber(vector<int>& nums) {
        string ans = "";
        sort(nums.begin(), nums.end(), cmp);

        for (auto i: nums) {
            ans += std::to_string(i);
        }

        if (ans[0] == '0') return "0";
        return ans;
    }
};
