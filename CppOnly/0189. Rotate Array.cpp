
/**
 *  把 array 後面要搬的直接拆開移到前面去
 *  多用空間但看起來簡單很多
 */


class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n=nums.size();
        k %= n;
        vector<int> front(nums.begin(), nums.begin()+n-k);
        vector<int> end(nums.begin()+n-k, nums.end());
        
        end.insert(end.end(), front.begin(), front.end());
        nums = end;
        return;
    }
};
