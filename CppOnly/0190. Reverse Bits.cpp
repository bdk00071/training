/**
    不要被騙，是 reverse 31 bits
    所以會多放一個 nCount = 31
 */


 class Solution {
public:
    uint32_t reverseBits(uint32_t n) {
        int nCount = 31;
        uint32_t nAns = 0, bit;
        while(nCount >= 0) {
            nAns = nAns *2 + n%2;
            n = n >> 1;
            nCount--;
        }

        return nAns;
    }
};