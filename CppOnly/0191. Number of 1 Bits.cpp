// 暴力算


class Solution {
public:
    int hammingWeight(int n) {
        int nCount = 0;
        while (n) {
            if (n%2) nCount++;
            n = n >> 1;
        }

        return nCount;
    }
};