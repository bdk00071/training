
/*
    這是個 DP 題， DP[i] 是偷取範圍在 vector[0] ~ vector[i] 的最大值
    那要填 vector[i+1] 的方法是偷取範圍在 vector[0] ~ vector[i-1]，或者 vector[0] ~ vector[i-2] + nums[i]
    兩個取最大值就是該填進去的。
*/

class Solution {
public:
    int rob(vector<int>& nums) {
        int n = nums.size(), RobbedMax[n];

        for (int i=0; i<n; i++) {
            if (i == 0) {
                RobbedMax[i] = nums[i];
            } else if (i==1) {
                RobbedMax[i] = max(nums[i], nums[i-1]);
            } else {
                RobbedMax[i] = max(RobbedMax[i-1], RobbedMax[i-2] + nums[i]);
            }
        }

        if (n==1) return RobbedMax[n-1];
        return max(RobbedMax[n-1], RobbedMax[n-2]);
    }
};
