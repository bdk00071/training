/**
 *  有一題要拿 tree 的 level order
 *  用相同走法就好處理
 */

class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        int i, n;
        vector<int> ans = {};
        vector<TreeNode*> parent = {}, child = {};

        if (root != NULL) parent.push_back(root);
        while (parent.size() != 0) {
            n = parent.size();
            for (i=0; i<n; i++) {
                if (parent[i]->left  != NULL) child.push_back(parent[i]->left);
                if (parent[i]->right != NULL) child.push_back(parent[i]->right);
                if (i==n-1) ans.push_back(parent[i]->val);
            }
            parent = child;
            child = {};
        }

        return ans;
    }
};