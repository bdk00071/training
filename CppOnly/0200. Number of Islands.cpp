/**
    土法煉鋼
    兩層 for loop 檢查有沒有 island
    有 island 時清掉並 +1
    clear 的動作會像病毒一樣把周遭四格一起吃掉
    遞迴繼續吃到沒有東西吃為止
 */


 class Solution {
public:
    void clear(vector<vector<char>>& grid, int i, int j) {
        if (i<0 || i>=grid[0].size() || j<0 || j>=grid.size()) return;
        if (grid[j][i] == '1') {
            grid[j][i] = '0';
            clear(grid, i, j+1);
            clear(grid, i, j-1);
            clear(grid, i+1, j);
            clear(grid, i-1, j);
        }
    }

    int numIslands(vector<vector<char>>& grid) {
        int ans=0;
        for (int j=0; j<grid.size(); j++) {
            for (int i=0; i<grid[0].size(); i++) {
                if (grid[j][i] == '1') {
                    clear(grid, i, j);
                    ans++;
                }
            }
        }
        return ans;
    }
};