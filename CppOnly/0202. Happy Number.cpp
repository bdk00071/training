
/**
    會出現 "沒辦法回到 1" 的狀況必定是出現 cycle
    所以能用 2 ptrs 的方法處理
    一個快一個慢，有碰到就是沒辦法到 1
 */




class Solution {
public:
    int nxt(int n) {
        int nNum = 0;
        while (n != 0) {
            int digit = n % 10;
            n = n / 10;
            nNum += digit * digit;
        }
        return nNum;
    }
    bool isHappy(int n) {
        if (n==1) return true;
        int slower = n, faster = nxt(n);

        while (slower != faster) {
            if (slower == 1 || faster==1) return true;
            slower = nxt(slower);
            faster = nxt(nxt(faster));
        }
        return false;
    }
};