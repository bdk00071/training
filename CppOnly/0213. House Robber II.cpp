
/*
    跟第一題一樣但不能同時偷頭尾房 => solution 會出現在 [0, n-1] 或 [1, n] 上
    都算過取比較大的。
*/

class Solution {
public:
    int rob(vector<int>& nums) {
        int n = nums.size();

        if (n==1) return nums[0];
        else if (n==1) return max(nums[0], nums[1]);

        vector<int> a(nums.begin(), nums.begin() + n-1);
        vector<int> b(nums.begin()+1, nums.end());
        
        return max(solve(a), solve(b));
    }

    int solve(vector<int>& nums) {
        int n = nums.size(), RobbedMax[n];

        for (int i=0; i<n; i++) {
            if (i == 0) {
                RobbedMax[i] = nums[i];
            } else if (i==1) {
                RobbedMax[i] = max(nums[i], nums[i-1]);
            } else {
                RobbedMax[i] = max(RobbedMax[i-1], RobbedMax[i-2] + nums[i]);
            }
        }

        if (n==1) return RobbedMax[n-1];
        return max(RobbedMax[n-1], RobbedMax[n-2]);
    }
};
