

/*
    塞進去排序，然後把不要的丟了
    也可以直接排序後拿 nums[nums.size() - k] 就是答案
*/

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        priority_queue<int> pq(nums.begin(),nums.end());
        while (k!=1) {
            pq.pop();
            k--;
        }
        return pq.top();
    }
};
