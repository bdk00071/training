/*
    backtrack 經典題
    {1,2...9} 中取 k 個數字，總和為 n 的 set 有幾個？
    那可以用相同的走法找資料
    walk() 的 idx 記目前看到 1~9 中哪個數字， k, n 是題目敘述的資料
    ans 是要 return 的答案， tmp 是要塞進 ans 的 vector， 而 sum 是 tmp 中元素總和

    walk() 設計概念是走到哪個 element 時檢查目前 sum 與 tmp （是否該放到 ans）
    如果該放或 sum 已大於 n 表示做完動作就可以結束動作。
    都不成立的話就繼續找：現在的　idx 可以選擇要或不要放入 tmp
    所以會再 call 兩次 walk()
    兩次只有 tmp, sum 會修改，其餘參數都相同
    如此可獲得正確解答。
*/



class Solution {
public:
    vector<vector<int>> combinationSum3(int k, int n) {
        vector<int> tmp;
        vector<vector<int>> ans;

        walk(1, k, n, ans, tmp, 0);
        return ans;
    }

    void walk(int idx, int k, int n, vector<vector<int>>& ans, vector<int> tmp, int sum) {
        if (idx > 10) return;

        if (sum == n && tmp.size() == k) {
            ans.push_back(tmp);
            return;
        }

        if (tmp.size() < k) {
            walk(idx+1, k, n, ans, tmp, sum);
            tmp.push_back(idx);
            walk(idx+1, k, n, ans, tmp, sum+idx);
        }

        return;
    }
};
