

// 暴力解，直接檢查附近是否有一樣的數字。

class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
        int n=nums.size(), i, j;

        for (i=0; i<n; i++) {
            for (j=1; j<=k && i+j<n; j++) {
                if (nums[i] == nums[i+j]) return true;
            }
        }

        return false;
    }
};