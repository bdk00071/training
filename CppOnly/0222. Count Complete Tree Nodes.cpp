/**
    給一個 Complete Tree
    算出 nodes 數量
    有特殊算法，用 inorder 可以解決
 */


 class Solution {
    int ans = 0;
    void inorder(TreeNode *root) {
        if (!root) return;
        inorder(root->left);
        ans++;
        inorder(root->right);
    }
public:
    int countNodes(TreeNode* root) {
        inorder(root);
        return ans;
    }
};