
// 這樣寫速度比較快，可能因為 if 的比較少

class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
        return walk(root);
    }

    TreeNode* walk(TreeNode* root) {
        if (root != NULL) {
            swap(root->left, root->right);
            walk(root->left);
            walk(root->right);
        }

        return root;
    }
};