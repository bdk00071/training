/**
    string converts()   讓 solution 好看點
    for loop 裡面只需要一個 flag 記 start 是誰就好
    會用到 nums[i-1] 所以這 loop 起點是 i=1，開跑前給好 nStart = nums[0]
    loop 跑完要自己寫好最後一個 range
 */


class Solution {
public:
    string converts(int start,int end) {
        if (start == end) {
            return to_string(start);
        } else {
            return to_string(start) + "->" + to_string(end);
        }
    }

    vector<string> summaryRanges(vector<int>& nums) {
        vector<string> ans;
        int n = nums.size();
        if (n==0) return ans;
        int nStart = nums[0];

        for (int i=1; i<n; i++) {
            if (nums[i] != nums[i-1]+1) {
                ans.push_back(converts(nStart, nums[i-1]));
                nStart = nums[i];
            }
        }
        ans.push_back(converts(nStart, nums[n-1]));
        return ans;
    }
};