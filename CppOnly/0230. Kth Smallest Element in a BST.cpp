// 用 inorder 倒出來找即可


class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        vector<int> ans;
        inorder(root, ans);
        return ans[k-1];
    }

    void inorder(TreeNode* root, vector<int>& ans) {
        if (root==NULL) return;
        inorder(root->left,  ans);
        ans.push_back(root->val);
        inorder(root->right, ans);
    }
};
