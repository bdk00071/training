/**
        沒事不要用 % 跟 /
        用乘法會比較快
 */


 class Solution {
public:
    bool isPowerOfTwo(int n) {
        int tmp = 1;
        for (int i=0; i<30; i++) {
            if (tmp == n) return true;
            tmp *= 2;
        }
        if (tmp == n) return true;
        return false;
    }
};