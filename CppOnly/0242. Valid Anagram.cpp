
/**
    兩個 string 排序後一樣就是 true，反之
    空間換時間的話可以用 umap 數 a-z 分別有多少
 */

class Solution {
public:
    bool isAnagram(string s, string t) {
        sort(s.begin(), s.end());
        sort(t.begin(), t.end());

        return s==t;
    }
};