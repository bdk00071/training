/*
    跟之前做 targetSum 類似概念，有路就走下去
    沒得走就存起來
*/


class Solution {
public:
    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> ans;

        paths(root, ans, "");
        return ans;
    }

    void paths(TreeNode* root, vector<string>& ans, string tmp) {
        if (root == NULL) {
            return;
        }

        if (tmp == "") {
            tmp = std::to_string(root->val);
        } else {
            tmp += "->" + std::to_string(root->val);
        }

        if (root->left == NULL && root->right == NULL) {
            ans.push_back(tmp);
            return;
        }

        if (root->left != NULL)  paths(root->left,  ans, tmp);
        if (root->right != NULL) paths(root->right, ans, tmp);
        return;
    }
};
