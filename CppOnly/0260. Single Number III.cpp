/**
    vector 多個數字，每個數字有兩個
    找出 single 的兩個數字
    只想得出 dictionary 解法

    底下解法看完 solution 概念寫
    XOR 先清掉兩兩一組的之後是兩個 ans 數字的 XOR 計算結果
    要怎麼利用這數字是關鍵
    因為已經保證是不同數字所以底下的 find 絕對不會是 0
    這個數字要怎麼用？拆成bits來看
    是０的bits沒用，我們不知道nums1, nums2應該要是 1 還是 2
    是１的很有用，這表示兩個數字在這 bit 不同
    用這 bit把nums的數字分組然後做XOR就一定是答案
    （不是答案的會兩兩放在同組消除）
 */


class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int find = 0, x, num1 = 0, num2 = 0;
        for (auto num : nums) find ^= num;
        for (int i=0; i<32; i++) if (1<<i & find) x = i;
        for (auto num : nums) {
            if (1<<x & num) num1 ^= num;
            else num2 ^= num;
        }

        return {num1,num2};
    }
};