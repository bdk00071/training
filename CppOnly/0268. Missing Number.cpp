/**
    簡單的數學計算
    全部有 n 個不同數字但有 n+1 格
    n+1 的數字的總合算好減去 nums 所有數字就好
 */


 class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int sum = 0, n = nums.size();

        for (int i=0; i<n; i++) {
            sum += nums[i];
        }

        return n*(n+1)/2 - sum;
    }
};