/*
    i = 目前指到哪個 index
    k = 非零的數要寫到哪個位置
    寫完後會知道 nums 尾要有多少個 0 就結束

*/

class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int n = nums.size(), k=0, i;

        for (i=0; i<n; i++) if (nums[i] != 0) nums[k++] = nums[i];
        
        i=n-1;
        while(i>=k) nums[i--]=0;
    }
};