/**
    這跟 power of two 是一樣的做法
    只需要先求 3^x 會小於 INT_MAX 的最大 x 就好
 */


 class Solution {
public:
    bool isPowerOfThree(int n) {
        if (n<0) return false;

        int tmp = 1;
        for (int i=0; i<19; i++) {
            if (tmp == n) return true;
            tmp *= 3;
        }
        if (tmp == n) return true;
        return false;
    }
};