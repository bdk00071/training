
/*
    觀察 n 轉成 2 進位時跟之前數字變化可以得知 bits 數可以用個位數區分 (n%2)
    如果 n 是偶數，那 bits 數跟 n/2 是一樣的
    奇數的話 n 的 bits 數會是 (n-1) 的加一
*/



class Solution {
public:
    vector<int> countBits(int n) {
        if (n==0) return {0};
        vector<int> ans = {0};
        for (int i=1; i<=n; i++) {
            if (i<=2) {
                ans.push_back(1);
            } else if (i%2 == 0) {
                ans.push_back(ans[i/2]);
            } else {
                ans.push_back(ans[i-1]+1);
            }
        }
        return ans;
    }
};
