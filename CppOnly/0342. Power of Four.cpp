/**
    與 porwer of two 相同
 */

 class Solution {
public:
    bool isPowerOfFour(int n) {
        if (n<0) return false;

        int tmp = 1;
        for (int i=0; i<15; i++) {
            if (tmp == n) return true;
            tmp *= 4;
        }
        if (tmp == n) return true;
        return false;
    }
};