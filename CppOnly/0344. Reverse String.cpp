class Solution {
public:
    void reverseString(vector<char>& s) {
        // reverse(s.begin(), s.end());
        int n=s.size(), i, tmp;
        for (i=0; i<n/2; i++) {
            tmp = s[i];
            s[i] = s[n-1-i];
            s[n-1-i] = tmp;
        }
    }
};