/**
    reverse 字串中的 vowels 
    好像蠻多類似題目
    
    reverse strings 是 swap char 後 left & right 都 ++ & -- 移動 index 1
    這種特定的就要去找
    left 的要 ++ 到下一個 vowel
    right的也要 -- 到下個 vowel

    其他類似題目都是這種解法
    一千多有一題是 reverse 大(還小)寫的

 */


 class Solution {
public:
    string reverseVowels(string s) {
        unordered_map<char, int> umap;
        int l = -1, r = s.length(), n = s.length();

        umap['a'] = 1; umap['A'] = 1;
        umap['e'] = 1; umap['E'] = 1;
        umap['i'] = 1; umap['I'] = 1;
        umap['o'] = 1; umap['O'] = 1;
        umap['u'] = 1; umap['U'] = 1;

        while (l < r) {
            l++;
            r--;
            while (l < n && umap[s[l]] != 1) l++;
            while (r >=0 && umap[s[r]] != 1) r--;
            if (l < r) swap (s[l], s[r]);
        }

        return s;
    }
};