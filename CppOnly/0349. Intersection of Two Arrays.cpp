用 umap 可以不這麼複雜




class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        vector<int> ans;
        unordered_map<int, int> umap;

        for (auto i : nums1) umap[i] = 1;
        for (auto i : nums2) {
            if (umap[i] == 1) {
                ans.push_back(i);
                umap[i] = 0;
            }
        }

        return ans;
    }
};
