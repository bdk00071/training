
/*
 *  前一題數字都只有一個，這次要處理數量
 */


class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        vector<int> ans;
        unordered_map<int, int> umap;

        for (auto &i : nums1) {
            umap[i]++;
        }

        for (auto &i : nums2) {
            if (umap[i] > 0) {
                ans.push_back(i);
                umap[i]--;
            }
        }
        return ans;
    }
};
