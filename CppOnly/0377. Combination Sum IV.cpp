
/*
    dp[10] 要記 10 怎麼湊出來的方法數，那 10 怎麼湊的?
    從 array 中看，10 - nums[i] >= 0 表示這個數有變化，應該要加上去
    for loop 跑完就把 dp[10] 算完了
    那 dp[i] 也是相同算法，獲得 dp[target] 後就是解
*/

class Solution {
public:
    int combinationSum4(vector<int>& nums, int target) {
        vector<unsigned int> dp(target + 1, 0);
        dp[0] = 1;

        for (int i = 1; i <= target; i++) {
            for (int j = 0; j < nums.size(); j++) {
                if (i >= nums[j]) {
                    dp[i] += dp[i - nums[j]];
                }
            }
        }

        return dp[target];
    }
};
