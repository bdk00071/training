
/*
    之前是加整條 path ，這次要記是往左還往右走。
    往左走 & 是葉子才需要加。
*/


class Solution {
public:
    int sumOfLeftLeaves(TreeNode* root) {
        int ans = 0;
        post(root, false, ans);
        return ans;
    }

    void post(TreeNode* root, bool left, int& ans) {
        if (root==NULL) return;
        post(root->left,  true, ans);
        post(root->right, false, ans);
        if (left==true && root->left==NULL && root->right==NULL) ans += root->val;
    }
};
