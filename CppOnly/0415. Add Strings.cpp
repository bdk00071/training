/**
    把加法實踐就好
 */

 class Solution {
public:
    string addStrings(string nums1, string nums2) {
        int i = nums1.size()-1, j = nums2.size()-1;
        int nSum = 0 , carry = 0;
        string ans;
        while (i >= 0 && j >= 0) {
            nSum = (nums1[i] - '0') + (nums2[j] - '0') + carry;
            ans.push_back(nSum%10 + '0');
            carry = nSum/10;
            i--;
            j--;
        }
        while (i >= 0) {
            nSum = nums1[i] - '0' + carry;
            ans.push_back(nSum%10 + '0');
            carry = nSum/10;
            i--;
        }
        while(j >= 0) {
            nSum = nums2[j] - '0' + carry;
            ans.push_back(nSum%10 + '0');
            carry = nSum/10;
            j--;
        }
        
        if (carry > 0) ans.push_back(carry + '0');
        reverse(ans.begin() , ans.end());
        return ans;
    }
};