/**
    進 loop 前先把 string 前面的空格清光
    進去後每次的動作都是 1, 找非空字 2, 清空白
    每次都可以讓 nCount ++
    這樣就做完了，前後有空白都是簡單處理掉
 */


 class Solution {
public:
    int countSegments(string s) {
        size_t pos;
        int nCount = 0;

        pos = s.find_first_not_of(" ");
        if (pos != string::npos) s = s.substr(pos);
        while (s.length() != 0) {
            if (s[0] != ' ') nCount++;

            pos = s.find(" ");
            if (pos == string::npos) break;
            s = s.substr(pos+1);

            pos = s.find_first_not_of(" ");
            if (pos != string::npos) s = s.substr(pos);
        }

        return nCount;
    }
};