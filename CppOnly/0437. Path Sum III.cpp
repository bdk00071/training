
/*
    起點只要比終點高，往下走就能走到的 path 就合法
    每次加完 val 就檢查結果是否符合 targetSum，有就放到 ans
    這樣就處理好終點。
    起點部分這樣想：
    int pathSum(TreeNode* root, int targetSum)
    這 function 是釋放起點的，我們讓它做遞迴
    在 root 放完第一輪後對 children 多放兩次，這樣整個 tree 的 nodes 就會全部檢查一次
*/




class Solution {
public:
    int ans=0;
    int pathSum(TreeNode* root, int targetSum) {
        if (root==NULL) return 0;

        walk(root, targetSum, 0);
        pathSum(root->left, targetSum);
        pathSum(root->right,targetSum);
        return ans;
    }

    void walk(TreeNode* root, int targetSum, long long tmp) {
        if (root == NULL) return;

        tmp += root->val;
        if (tmp == targetSum) ans++;
        walk(root->left,  targetSum, tmp);
        walk(root->right, targetSum, tmp);

        return;
    }
};
