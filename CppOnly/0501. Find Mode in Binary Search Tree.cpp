

/**
 * BST 找最多的數字，可能有重複
 * 那就把所有數字倒進 umap 然後看重複次數最多的就好
 */


 class Solution {
public:
    void inorder(TreeNode* root, unordered_map<int, int> &umap) {
        if (!root) return;
        
        inorder(root->left,  umap);
        umap[root->val]++;
        inorder(root->right, umap);
        return;
    }

    vector<int> findMode(TreeNode* root) {
        int max = 0;
        vector<int> ans;
        unordered_map<int, int> umap;

        inorder(root, umap);
        for (auto &i : umap) {
            if (i.second > max) max = i.second;
        }

        for (auto &i : umap) {
            if (i.second == max) ans.push_back(i.first);
        }

        return ans;
    }
};