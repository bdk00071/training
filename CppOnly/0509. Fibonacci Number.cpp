
// 費式數列。DP 教學題



class Solution {
public:
    int fib(int n) {
        int ans[3];
        ans[0] = 0;
        ans[1] = 1;
        for (int i=2; i<=n; i++) ans[i%3] = ans[(i-1)%3]+ans[(i-2)%3];
        return ans[n%3];
    }
};
