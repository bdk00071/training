
/*
    因為是要找 "底層最左邊 node 的 value"
    所以這題也是 tree level order 的延伸應用
    把 level order 的 codes 做修改就是 solution
 */




class Solution {
public:
    int findBottomLeftValue(TreeNode* root) {
        int i, n, ans;
        vector<TreeNode*> tree, children;

        if (root != NULL) tree.push_back(root);
        while (tree.size()!=0) {
            for (i=0; i<tree.size(); i++) {
                if (tree[i]->left != NULL)  children.push_back(tree[i]->left);
                if (tree[i]->right != NULL) children.push_back(tree[i]->right);
                if (i==0) ans = tree[i]->val;
            }
            tree = children;
            children = {};
        }
        return ans;
    }
};
