

/*
    要找 tree 上每一層 row 的最大 value
    只要能用 level order 走過一輪，找最大值並且存下來就不是難題
    level order 的走法請參考 102 題
 */


class Solution {
public:
    vector<int> largestValues(TreeNode* root) {
        int i, n, tmp;
        vector<int> ans;
        vector<TreeNode*> tree, children;

        if (root != NULL) tree.push_back(root);
        while (tree.size()!=0) {
            tmp = INT_MIN;
            for (i=0; i<tree.size(); i++) {
                if (tree[i]->left != NULL)  children.push_back(tree[i]->left);
                if (tree[i]->right != NULL) children.push_back(tree[i]->right);
                tmp = max(tmp, tree[i]->val);
            }
            ans.push_back(tmp);
            tree = children;
            children = {};
        }
        return ans;
    }
};
