
/**
    字串要是全大寫或全小寫或只有第一個字大寫
    那就三種types都檢查，有符合就return結束
 */


 class Solution {
public:
    bool detectCapitalUse(string word) {
        bool bDone;
        int n = word.length(), i;

        bDone = true;
        for (i=0; i<n; i++) { // 全小寫
            if (word[i] < 'a' || word[i] > 'z') {
                bDone = false;
                break;
            }
        }
        if (bDone == true) return true;
        
        bDone = true;
        for (i=0; i<n; i++) { // 全大寫
            if (word[i] < 'A' || word[i] > 'Z') {
                bDone = false;
                break;
            }
        }
        if (bDone == true) return true;

        bDone = true;
        if (word[0] > 'Z' || word[0] < 'A') return false;
        for (i=1; i<n; i++) {
            if (word[i] < 'a' || word[i] > 'z') {
                bDone = false;
                break;
            }
        }
        if (bDone == true) return true;
        
        return false;
    }
};