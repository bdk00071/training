/**
    找出最小的 difference
    因為是 BST 所以只要跟左右兩個比大小就好
    inorder 走一輪
    ans 記目前取得最小 difference， pre 記前一個 node
 */


 class Solution {
    int ans = INT_MAX;
    TreeNode* pre = NULL;
    
    void inorder(TreeNode* root) {
        if (root == NULL) return;
        inorder(root->left);
        if (pre != NULL) ans = min(ans, root->val-pre->val);
        pre = root;
        inorder(root->right);
    }

public:
    int getMinimumDifference(TreeNode* root) {
        inorder(root);
        return ans;
    }
};