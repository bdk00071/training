/**
    題目是給一個 tree，最長的 path 長度是多少
    抓到關鍵就不難想
    找 node 上左右兩邊 subtree 的 maxDepth 加起來最長就會是答案
    （因為他是tree，不會有cycle出現是重點）
    但這樣寫速度會頗慢的= = 
 */

 class Solution {
    int maxDepth(TreeNode* root) {
        if (root==NULL) return 0;
        return help(root, 0);
    }
    int help(TreeNode* root, int lv) {
        if (root->left == NULL && root->right == NULL) return lv+1;

        if (root->left  == NULL) return help(root->right, lv) +1;
        if (root->right == NULL) return help(root->left,  lv) +1;

        return max(help(root->left,  lv), help(root->right, lv))+1;
    }
    int ans = 0;
public:
    int diameterOfBinaryTree(TreeNode* root) {
        if (root == NULL) return 0;
        int l, r, tmp = 0;

        l = maxDepth(root->left);
        r = maxDepth(root->right);
        if (l != 0) tmp = tmp + l;
        if (r != 0) tmp = tmp + r;
        ans = max(tmp, ans);

        diameterOfBinaryTree(root->left);
        diameterOfBinaryTree(root->right);

        return ans;
    }
};