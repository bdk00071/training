/**
    tmp 是字串開頭
    i 是字串結尾
    第一個 while 還沒碰到尾時表示還有字串還沒 reverse
    第二個 while 要把 i 移到空格上
    然後把 tmp ~ i 做 reverse 就完成一個 word
    tmp = i+1 就會是下一個字的開頭
    i = tmp 就完成下次 while 的初始化
 */


 class Solution {
public:
    string reverseWords(string s) {
        int tmp = 0, i = 0, n = s.length();
        while (tmp < n) {
            while (i < n && s[i] != ' ') i++;
            reverse(s.begin() + tmp, s.begin() + i);
            tmp = i+1;
            i = tmp;
        }
        return s;
    }
};