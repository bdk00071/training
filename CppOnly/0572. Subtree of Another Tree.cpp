// 比較而已


class Solution {
public:
    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
        if (!root) return false;

        if (root->val == subRoot->val 
                && true == isSameTree(root, subRoot)) {
            return true;
        }

        return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
    }

    bool isSameTree(TreeNode* p, TreeNode* q) {
        if (p==NULL && q!=NULL) return false;
        if (p!=NULL && q==NULL) return false;
        if (p==NULL && q==NULL) return true;
        if (p->val != q->val) return false;
        if (isSameTree(p->left, q->left) == false) return false;
        if (isSameTree(p->right, q->right) == false) return false;

        return true;
    }
};