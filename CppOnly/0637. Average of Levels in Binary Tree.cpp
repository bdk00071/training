/**
  level order 改一下就能把這個算好
*/


class Solution {
public:
    vector<double> averageOfLevels(TreeNode* root) {
        int i;
        double nSum;
        vector<double> ans;
        vector<TreeNode*> parent, child;

        parent.push_back(root);
        while (parent.size() != 0) {
            nSum = 0;
            for (i=0; i<parent.size(); i++) {
                // 算 sum & 放 child
                nSum += parent[i]->val;
                if (parent[i]->left != NULL) child.push_back(parent[i]->left);
                if (parent[i]->right != NULL) child.push_back(parent[i]->right);
            }
            ans.push_back((double)nSum/parent.size());
            parent = child;
            child = {};
        }

        return ans;
    }
};