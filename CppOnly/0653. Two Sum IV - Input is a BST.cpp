/**
    用 umap 記 "有那些數字"
    就是 0001 的空間換時間寫法
 */

 class Solution {
    unordered_map<int, int> umap;
public:
    bool findTarget(TreeNode* root, int k) {
        if (root == NULL) return false;

        if (umap[k - root->val] == 1) return true;
        umap[root->val] = 1;

        if (findTarget(root->left,  k) == true) return true;
        if (findTarget(root->right, k) == true) return true;
        
        return false;
    }
};