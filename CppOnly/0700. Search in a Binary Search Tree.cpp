
/**
    BST 找數字，依照 BST 規則找就好
 */

class Solution {
public:
    TreeNode* searchBST(TreeNode* root, int val) {
        if (root == NULL || root->val == val) return root;

        if (val < root->val) return searchBST(root->left, val);
        else if (root->val < val) return searchBST(root->right, val);
        
        return NULL;
    }
};