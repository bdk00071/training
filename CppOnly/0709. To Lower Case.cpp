/**
    大寫轉小寫
    是大寫就 +32 移過去就好
 */


 class Solution {
public:
    string toLowerCase(string s) {
        int n = s.length();

        for (int i=0; i<n; i++) {
            if (s[i] >= 'A' && s[i] <= 'Z') s[i] += 32;
        }

        return s;
    }
};