/**
    只比較樹葉的 value 順序
    那就用 inorder 走一次 & leaves 留下就可以完成
 */

class Solution {
    void getLeaf(TreeNode* root, vector<int>& Leaf) {
        if (root == NULL) return;
        getLeaf(root->left, Leaf);
        if (root->left == NULL && root->right == NULL) Leaf.push_back(root->val);
        getLeaf(root->right, Leaf);
        return;
    }
public:
    bool leafSimilar(TreeNode* root1, TreeNode* root2) {
        int n;
        vector<int> leaf1 = {}, leaf2 = {};
        
        getLeaf(root1, leaf1);
        getLeaf(root2, leaf2);
        if (leaf1.size() != leaf2.size()) return false;
        n = leaf1.size();
        for (int i=0; i<n; i++) {
            if (leaf1[i] != leaf2[i]) return false;
        }
        return true;
    }
};