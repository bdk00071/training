/**
    已經很直接說用 inorder 順序
    那就走一輪重整就好
 */

class Solution {
    TreeNode *head = new TreeNode(0), *current = head, *tmp;
    void inorder(TreeNode* root) {
        if (root == NULL) return;
        inorder(root->left);
        current->right = root;
        current = root;
        root->left = NULL;
        tmp = root->right;
        root->right = NULL;
        
        inorder(tmp);
    } 
public:
    TreeNode* increasingBST(TreeNode* root) {
        inorder(root);
        return head->right;
    }
};