/**
    給 BST 跟 [low, high]
    要算 BST 上這 range 的 sum
    當然可以土法煉鋼的算。
    但都給 BST 當然要用好他的特性
    如果 root->val 還在 range 裡面，那 left, right 都還有機會加進來
    root->val 已經比 low 低的話 left 就不需要繼續跑
    high => 不要跑 right
 */


class Solution {
public:
    int rangeSumBST(TreeNode* root, int low, int high) {
        int ans = 0;
        if (root == NULL) return ans;
        
        if (low <= root->val && root->val <= high) {
            ans += root->val;
            ans += rangeSumBST(root->left,  low, high);
            ans += rangeSumBST(root->right, low, high);
        } else if (root->val < low) {
            ans += rangeSumBST(root->right, low, high);
        } else if (root->val > high) {
            ans += rangeSumBST(root->left,  low, high);
        }
        
        return ans;
    }
};