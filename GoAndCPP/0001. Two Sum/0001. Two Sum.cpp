
// 基本題

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        int n=nums.size();
        for (int i=0; i<n; i++) {
            for (int j=i+1; j<n; j++) {
                if (nums[i] + nums[j] == target) {
                    return {i, j};
                }
            }
        }

        return {0, 1};
    }
};

/*
    也可以用空間換時間：
    一個 for loop 檢查 unordered_map 內是否有 target - nums[i]
    有就表示找到解了，沒有解就把 nums[i] 放進去。
*/

class Solution {
public:
    vector<int> twoSum(std::vector<int>& nums, int target) {
        unordered_map<int, int> umap;
        vector<int> result;
        int complement;
        for (int i = 0; i < nums.size(); i++) {
            complement = target - nums[i];
            if (umap.find(complement) != umap.end()) {
                result.push_back(umap[complement]);
                result.push_back(i);
                break;
            }
            umap[nums[i]] = i;
        }
        
        return result;
    }
};
