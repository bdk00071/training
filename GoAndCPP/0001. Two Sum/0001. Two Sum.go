func twoSum(nums []int, target int) []int {

	i, j, n := 0, 0, len(nums)
	for i = 0; i < n-1; i++ {
		for j = i + 1; j < n; j++ {
			if nums[i]+nums[j] == target {
				return []int{i, j}
			}
		}
	}

	return []int{0, 0}
}