/*
    兩個 linkedlists 表示兩個數字， return 相加後的 linkedlist.
    要一個 dummy ，最高位數有進位時需要多一個 node，其他都用原本就在的 nodes
    速度有點慢，判斷太多。
*/

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int nTen = 0;
        ListNode *dummy = new ListNode(0), *current = dummy;

        while (nTen || l1 || l2) {
            if (l1 && l2) {
                current->next = l1;
                l1->val += l2->val + nTen;
                nTen = l1->val/10;
                l1->val %= 10;
            } else if (l1) {
                current->next = l1;
                l1->val += nTen;
                nTen = l1->val/10;
                l1->val %= 10;
            } else if (l2) {
                current->next = l2;
                l2->val += nTen;
                nTen = l2->val/10;
                l2->val %= 10;
            } else if (nTen) {
                current->next = new ListNode(nTen);
                nTen = 0;
            }
            if (l1) l1 = l1->next;
            if (l2) l2 = l2->next;
            current = current->next;
            current->next = NULL;
        }

        return dummy->next;
    }
};
