// Given an integer x, return true if x is a palindrome, and false otherwise.
// 把數字反過來之後要多個檢查。


class Solution {
public:
    bool isPalindrome(int x) {
        int y = x;
        if (x < 0) return false;
            
        long int temp=0;
        while(y) {
            temp = temp*10 + y%10;
            y /= 10;
        }

        if (temp==x) return true;
        return false;
    }
};
