
看其他說明後自己寫
先清掉一些後面演算不能處理的東西
然後是 tmp 紀錄目前從 x 反轉過來多少數字
如果 x == tmp 那表示 x 原本是偶個位數 & reverse 後會相同
如果 x 清掉一個位數跟 tmp 相同那位數就是奇數



func isPalindrome(x int) bool {
	if x < 10 && x >= 0 {
		return true
	} else if x < 0 || x%10 == 0 {
		return false
	}

	tmp := 0
	for x != 0 {
		tmp = tmp*10 + x%10
		x /= 10
		if tmp == x || tmp == x/10 {
			return true
		}
	}

	return false
}