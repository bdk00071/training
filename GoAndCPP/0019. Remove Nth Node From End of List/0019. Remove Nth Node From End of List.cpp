/*
    第一步是要想辦法拿到從尾端數來第 n 個 node
    土法煉鋼是先數 list 總數後得知要刪第幾個
    換個方式：一個指標先往前走 n 個 nodes
    另一個才從 head 出發，當先出發的抵達 "最後一個" 時
    晚出發的會在指定刪除的 node 前一個上，這時把 next 修改掉即可
*/

class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode *f = head , *s = head;
        while(n != 0) {
            n--;
            f = f->next;
        }

        if (f == NULL) return head->next;

        while (f->next != NULL) {
            f = f->next;
            s = s->next;
        }
        
        s->next = s->next->next;
        return head;
    }
};
