// 單純的題目

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
        if (!list1) return list2;
        else if (!list2) return list1;

        ListNode *dummy = new ListNode(0), *current = dummy;

        while (list1 || list2) {
            if (list1 && list2 && list2->val < list1->val) {
                current->next = list2;
                list2 = list2->next;
                current = current->next;
                current->next = NULL;
            } else if (list1 && list2 && list1->val <= list2->val) {
                current->next = list1;
                list1 = list1->next;
                current = current->next;
                current->next = NULL;
            } else if (list1) {
                current->next = list1;
                list1 = NULL;
            } else if (list2) {
                current->next = list2;
                list2 = NULL;
            }
        }

        return dummy->next;
    }
};