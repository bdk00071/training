func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy

	if list1 == nil {
		return list2
	} else if list2 == nil {
		return list1
	}

	for list1 != nil || list2 != nil {
		if list1 == nil {
			current.Next = list2
			break
		} else if list2 == nil {
			current.Next = list1
			break
		} else if list1.Val <= list2.Val {
			current.Next = list1
			current = current.Next
			list1 = list1.Next
		} else if list2.Val < list1.Val {
			current.Next = list2
			current = current.Next
			list2 = list2.Next
		}
		current.Next = nil
	}

	return dummy.Next
}