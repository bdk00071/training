

/*
    兩個兩個 node 互換 ~
    沒什麼特別， list 特性掌握好就 OK
*/

class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        ListNode *dummy = new ListNode, *current = dummy, *tmp;

        while (head) {
            if (head->next == NULL) {
                current->next = head;
                head = head->next;
            } else {
                tmp = head;
                head = head->next->next;
                current->next = tmp->next;
                current = current->next;
                current->next = tmp;
                current = current->next;
                current->next = NULL;
            }
        }

        return dummy->next;
    }
};
