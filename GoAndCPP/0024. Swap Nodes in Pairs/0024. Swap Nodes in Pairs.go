func swapPairs(head *ListNode) *ListNode {
	if head == nil {
		return head
	}

	dummy := new(ListNode)
	current := dummy
	for head != nil {
		left := head
		head = head.Next
		if head != nil {
			current.Next = head
			head = head.Next
			current = current.Next
		}

		current.Next = left
		current = current.Next
		current.Next = nil
	}

	return dummy.Next
}