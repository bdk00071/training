
/*
    先數過整個 list 數量，把 k 多餘次數 % 掉
    它需要搬幾次我們就讓 faster 指針先跑幾次
    之後 current 指向 head，兩個同步移動到 faster 是 list 尾端
    此時 current 後面有 k%count 個 nodes
    再把 list 調整成我們要的就結束
*/

class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        if (!head) return head;
        int count = 0;
        ListNode* current = head, *faster = head;

        while (current) {
            current = current->next;
            count++;
        }
        k %= count;

        while (k != 0) {
            faster = faster->next;
            k--;
        }

        current = head;
        while (faster->next != NULL) {
            faster = faster->next;
            current = current->next;
        }
        faster->next = head;
        faster = current->next;
        current->next = NULL;

        return faster;
    }
};
