/**
  跟 CPP 不同的寫法
  reverse 後就會變成把 head 放到 tail，動作簡單很多
  但依然要有的需求是 "算長度然後把 K 減少"
  不然多餘的動作太多了
*/

func reverseList(head *ListNode) (*ListNode, int) {
	dummy := new(ListNode)
	current := dummy
	tmp := current
	var len int = 0

	for head != nil {
		tmp = head
		head = head.Next
		tmp.Next = dummy.Next
		dummy.Next = tmp
		len++
	}

	return dummy.Next, len
}

func rotateRight(head *ListNode, k int) *ListNode {
	if k == 0 || head == nil || head.Next == nil {
		return head
	}
	head, len := reverseList(head)
	k %= len

	finalOne, tmp := head, head
	for finalOne.Next != nil {
		finalOne = finalOne.Next
	}

	for k != 0 {
		tmp = head
		head = head.Next
		tmp.Next = nil
		finalOne.Next = tmp
		finalOne = finalOne.Next
		k--
	}

	head, _ = reverseList(head)
	return head
}