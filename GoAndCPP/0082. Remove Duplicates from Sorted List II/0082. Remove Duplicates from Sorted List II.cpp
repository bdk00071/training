
/*
    很多要 "刪除" 的 linked list 題目呀
    先找到重複的之後就可以去刪，
    刪的方法可以調整原本 list 的 ptr 或者不要放到 new list
*/


class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if (!head) return head;

        int remove;
        ListNode* dummy = new ListNode, *current = dummy;

        while (head) {
            if (head->next != NULL && head->val == head->next->val) {
                remove = head->val;
                while (head != NULL && head->val == remove) head = head->next;
            } else {
                current->next = head;
                current = current->next;
                head = head->next;
                current->next = NULL;
            }
        }

        return dummy->next;
    }
};
