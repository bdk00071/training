

func deleteDuplicates(head *ListNode) *ListNode {
	if head == nil {
		return head
	}

	var nDupe = 0
	dummy := new(ListNode)
	current := dummy

	for head != nil {
		if head.Next != nil && head.Next != nil && head.Val == head.Next.Val {
			nDupe = head.Val
			for head != nil && head.Val == nDupe {
				head = head.Next
			}
		} else {
			current.Next = head
			head = head.Next
			current = current.Next
			current.Next = nil
		}
	}

	return dummy.Next
}
