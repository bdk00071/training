
/*
    這題跟 0082 差別是這題只要刪到 "不重複" 就好
    （0082要把重複的全部刪掉）
    全部刪乾淨是比較麻煩的。
    可以明顯看到這題 codes 少非常多
*/

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if (!head || head->next == NULL) return head;

        int value;
        ListNode *current = head;

        while (current && current->next != NULL) {
            if (current->val == current->next->val) {
                current->next = current->next->next;
            } else {
                current = current->next;
            }
        }

        return head;
    }
};

