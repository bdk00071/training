

func deleteDuplicates(head *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy

	dummy.Val = -10000
	for head != nil {
		if current.Val != head.Val {
			current.Next = head
			head = head.Next
			current = current.Next
			current.Next = nil
		} else {
			head = head.Next
		}
	}

	return dummy.Next
}