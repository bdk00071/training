/*
    給一個分界點，把原有的 list 分前後兩邊。
    簡易的思考是我開兩個 dummyHead 各自接收兩個 partitions
    處理完後把兩條 lists 接起來。
*/

class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        ListNode *bHead = new ListNode, *curBig = bHead, *sHead = new ListNode, *curSmall = sHead, *current = head;
        
        while (current) {
            if (current->val >= x) {
                curBig->next = current;
                current = current->next;
                curBig = curBig->next;
                curBig->next = NULL;
            } else {
                curSmall->next = current;
                current = current->next;
                curSmall = curSmall->next;
                curSmall->next = NULL;
            }
        }

        curSmall->next = bHead->next;
        bHead->next = NULL;
        delete(bHead);

        return sHead->next;
    }
};
