func partition(head *ListNode, x int) *ListNode {
	smallDummy, bigDummy := new(ListNode), new(ListNode)
	currentSmall, currentBig := smallDummy, bigDummy

	for head != nil {
		if head.Val >= x {
			currentBig.Next = head
			head = head.Next
			currentBig = currentBig.Next
			currentBig.Next = nil
		} else {
			currentSmall.Next = head
			head = head.Next
			currentSmall = currentSmall.Next
			currentSmall.Next = nil
		}
	}

	currentSmall.Next = bigDummy.Next
	return smallDummy.Next
}