
/*
    不 reverse 整條，而是中間一段。

    先把整條 list 接上 dummy 後面
    把 left 移動需要搬的 node 前一個
    下個 for loop 數量是需要搬動的 nodes 數量
    一個一個搬去 prev 後面，這樣就可以達到題目要求
*/



class Solution {
    public:
        ListNode *reverseBetween(ListNode *head, int left, int right) {
            auto dummy = make_unique<ListNode>(0, head);
            auto prev = dummy.get(), curr = dummy.get();
            for (int i = 0; i < left - 1; ++i) {
                prev = prev->next;
            }
            curr = prev->next;
            for (int i = 0; i < right - left; ++i) {
                auto tmp = prev->next;
                prev->next = curr->next;
                curr->next = curr->next->next;
                prev->next->next = tmp;
            }
            
            return dummy->next;
    }
};
