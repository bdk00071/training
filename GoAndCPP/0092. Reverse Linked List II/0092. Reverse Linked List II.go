/*
   與 CPP 不同的做法
   用 idx 記現在處理第幾個 node
   到範圍內時換擺放方式
   reverse 最後一個要把 current 推到最後一個
   所以方式有三種：放後面然後馬上往後走、放自己後面然後不往後、放後面然後走到最後
*/
func reverseBetween(head *ListNode, left int, right int) *ListNode {
	dummy := new(ListNode)
	current := dummy
	var idx int = 0

	for head != nil {
		idx++
		if idx < left || right < idx {
			current.Next = head
			head = head.Next
			current = current.Next
			current.Next = nil
		} else if idx == right {
			tmp := head
			head = head.Next
			tmp.Next = current.Next
			current.Next = tmp
			for current.Next != nil {
				current = current.Next
			}
		} else {
			tmp := head
			head = head.Next
			tmp.Next = current.Next
			current.Next = tmp
		}
	}

	return dummy.Next
}