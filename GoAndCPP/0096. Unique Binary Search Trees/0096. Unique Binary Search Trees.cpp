
/**
    觀察 BST 就知道可以用 DP
    除了 root 外 left、right 有幾個 node？
    這問題就是重點
 */


class Solution {
public:
    int numTrees(int n) {
        int *nums = (int *)malloc(sizeof(int)*(n+1)), i, j;
        
        for (i=0; i<=n; i++) nums[i] = 0;
        nums[0] = nums[1] = 1;
        
        for (i=2; i<=n; i++) {
            for (j=0; j<=i-1; j++) {
                nums[i] += nums[j] * nums[i-1-j];
            }
        }
        return nums[n];
    }
};