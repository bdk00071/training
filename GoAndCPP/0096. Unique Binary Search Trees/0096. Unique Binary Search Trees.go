func numTrees(n int) int {
	ans := make([]int, n+1)
	for i := 0; i <= n; i++ {
		ans[i] = 0
	}
	ans[0] = 1
	ans[1] = 1

	for i := 2; i <= n; i++ {
		for j := 0; j <= i-1; j++ {
			ans[i] += ans[j] * ans[i-1-j]
		}
	}

	return ans[n]
}
