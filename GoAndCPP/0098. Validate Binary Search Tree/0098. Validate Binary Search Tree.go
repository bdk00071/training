func inorder(root *TreeNode, ans *[]int) {
	if root == nil {
		return
	}
	inorder(root.Left, ans)
	*ans = append(*ans, root.Val)
	inorder(root.Right, ans)

	return
}

func isValidBST(root *TreeNode) bool {
	ans := make([]int, 0)
	inorder(root, &ans)
	for i := 1; i < len(ans); i++ {
		if ans[i] <= ans[i-1] {
			return false
		}
	}
	return true
}