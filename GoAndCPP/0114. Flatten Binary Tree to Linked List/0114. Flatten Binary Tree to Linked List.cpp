/**
    用遞迴寫
    概念大概是看這一個底下要怎麼搬動
    如果 root->left 是空的那就處理好 root->right 即可
    如果 root->right  空的，那把 left 搬去 right 處理完就好
    兩邊都有的話那動作就比較多，先找到 root->left 能往右走最多的 leaf
    這位置就是要接 root->right
    後面把 link 換一下就可以繼續跑 flatten(root->right)
 */


class Solution {
public:
    void flatten(TreeNode* root) {
        if (!root) return;
        TreeNode *current;

        if (root->left == NULL) {
            flatten(root->right);
        } else if (root->right == NULL) {
            root->right = root->left;
            root->left = NULL;
            flatten(root->right);
        } else {
            // 兩邊都有
            current = root->left;
            while (current->right != NULL) current = current->right;

            current->right = root->right;
            root->right = root->left;
            root->left = NULL;
            flatten(root->right);
        }
    }
};