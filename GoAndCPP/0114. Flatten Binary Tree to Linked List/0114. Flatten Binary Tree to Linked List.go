func flatten(root *TreeNode) {
	if root == nil {
		return
	}

	if root.Left == nil {
		flatten(root.Right)
	} else if root.Right == nil {
		root.Right = root.Left
		root.Left = nil
		flatten(root.Right)
	} else {
		current := root.Left
		for current.Right != nil {
			current = current.Right
		}
		current.Right = root.Right
		root.Right = root.Left
		root.Left = nil
		flatten(root.Right)
	}
}
