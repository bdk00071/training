

// 2 ptrs 一個快一個慢。如果沒 cycle 的話快的一定先到終點，有 cycle 的話一定會讓 2 ptrs 指向同一個 node



class Solution {
public:
    bool hasCycle(ListNode *head) {
        ListNode *f = head, *s = head;

        while (f != NULL) {
            if (f != NULL) f = f->next;
            if (f != NULL) f = f->next;
            if (s != NULL) s = s->next;
            if (f == s && f != NULL) return true;
        }
        return false;
    }
};
