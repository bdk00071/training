func hasCycle(head *ListNode) bool {
	if head == nil || head.Next == nil {
		return false
	}
	f, s := head, head
	f = head.Next

	for f != nil {
		if f == s {
			return true
		}
		s = s.Next
		f = f.Next
		if f != nil {
			f = f.Next
		}
	}

	return false
}