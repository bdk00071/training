/**

        一樣用 slow, fast 就可以解掉

        cycle 長度為 m:
        (1) slow 走 K 代表 fast 走 2K ，相遇代表 fast 比 slow 多繞了迴圈至少一次

        => (2K - K) = n * m

        (2) 一個人從起點開始走，另外一個人從相遇點開始走，會在 cycle node 相遇，原因是:

        從起點走到相遇點是 K，而 K = n * m (迴圈的倍數)，因此這兩個人至少一定會在剛剛的相遇點相遇。

        要能夠再相遇點相遇，則必定在 cycle node 就要遇到，才能夠一起走到相遇點

 */



class Solution {
public:
    ListNode *detectCycle(ListNode *head) {    
        ListNode *faster = head, *slower = head;
        
        while (true) {     
            if (faster != NULL && faster->next != NULL) {
                slower = slower->next; 
                faster = faster->next->next;
            } else {
                return NULL;
            }
            if (slower == faster) break;
        }

        while (slower != head) {
            slower = slower->next;
            head = head->next;    
        }
        return head;
    }
};