func detectCycle(head *ListNode) *ListNode {
	slow, fast := head, head

	for true {
		if fast != nil && fast.Next != nil {
			slow = slow.Next
			fast = fast.Next.Next
		} else {
			return nil
		}
		if slow == fast {
			break
		}
	}
	for slow != head {
		slow = slow.Next
		head = head.Next
	}
	return head
}
