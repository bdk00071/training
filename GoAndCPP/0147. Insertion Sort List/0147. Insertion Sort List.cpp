/*
    排序 Linked List
    把每個 Nodes 放入另一個 sorted list 時找對的位置
    或者每次都用 merge 去整理
    但這樣燒時間，而且 code 很長
*/

class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        ListNode *dummy = new ListNode, *nxt, *current;
        dummy->val = -100000;

        while(head) {
            current = dummy;
            nxt = dummy->next;
            while(nxt != NULL && head->val > nxt->val) {
                current = current->next;
                nxt = nxt->next;
            }
            current->next = head;
            head = head->next;
            current->next->next = nxt;
            current = current->next;
            if (nxt) nxt = nxt->next;
        }

        return dummy->next;
    }
};


class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        ListNode *ans = NULL, *tmp;

        while (head) {
            tmp = head;
            head = head->next;
            tmp->next = NULL;
            ans = merge(ans, tmp);
        }

        return ans;
    }

    ListNode* merge(ListNode* left, ListNode* right) {
        if (!left) return right;
        else if (!right) return left;

        ListNode *dummy = new ListNode(0), *current = dummy;

        while (left || right) {
            if (left && right) {
                if (left->val >= right->val) {
                    current->next = right;
                    right = right->next;
                } else {
                    current->next = left;
                    left = left->next;
                }
                current = current->next;
                current->next = NULL;
            } else if (left) {
                current->next = left;
                left = NULL;
            } else if (right) {
                current->next = right;
                right = NULL;
            }
        }
        
        return dummy->next;
    }
};