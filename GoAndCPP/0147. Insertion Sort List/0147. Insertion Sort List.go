
/*
   換正常寫法
   有資料的話找到 insert 位置後放進去
*/

func insertionSortList(head *ListNode) *ListNode {
	dummy, current := new(ListNode), head

	for current != nil {
		// 找 position
		prev := dummy
		next := dummy.Next
		for next != nil && current.Val > next.Val {
			prev = next
			next = next.Next
		}

		// insert
		currentNext := current.Next
		current.Next = next
		prev.Next = current
		current = currentNext
	}

	return dummy.Next
}