/**
 *      倒出來用 std 排序完放回去就好
 *      也可以自己用 merge sort 
 *      但注意這邊用 O(N^2) 的排序速度會太慢
 */


class Solution {
public:
    
    ListNode* sortList(ListNode* head) {
        int i=0;
        ListNode *current = head;
        vector<int> tmp;
        while (current) {
            tmp.push_back(current->val);
            current = current->next;
        }

        sort(tmp.begin(), tmp.end());
        current = head;
        while (current) {
            current->val = tmp[i++];
            current = current->next;
        }

        return head;
    }
};