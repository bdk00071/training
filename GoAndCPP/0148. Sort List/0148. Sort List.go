
/**
  土法煉鋼
*/

func sortList(head *ListNode) *ListNode {
	var tmp []int
	ans := head
	current := ans

	for current != nil {
		tmp = append(tmp, current.Val)
		current = current.Next
	}
	sort.Ints(tmp)
	current = ans
	idx := 0
	for current != nil {
		current.Val = tmp[idx]
		current = current.Next
		idx++
	}
	return ans
}