
func getIntersectionNode(headA, headB *ListNode) *ListNode {
	a, b := getLen(headA), getLen(headB)

	for a > b {
		a--
		headA = headA.Next
	}

	for b > a {
		b--
		headB = headB.Next
	}

	for headA != nil {
		if headA == headB {
			return headA
		}
		headA = headA.Next
		headB = headB.Next
	}

	return nil
}

func getLen(head *ListNode) int {
	ans := 0
	for head != nil {
		ans++
		head = head.Next
	}
	return ans
}