

// 刪 node 題

class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        if (head == NULL) return head;

        ListNode *current, *pre;

        while (head->val == val) {
            head = head->next;
            if (head == NULL) return head;
        }

        pre = head;
        current = head->next; // head-> val != val
        while (current != NULL) {
            if (current->val == val) {
                pre->next = current->next;
                current = current->next;
            } else {
                pre = pre->next;
                current = current->next;
            }
        }

        return head;
    }
};
