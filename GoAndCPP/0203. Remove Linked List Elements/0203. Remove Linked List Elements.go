func removeElements(head *ListNode, val int) *ListNode {
	dummy := new(ListNode)
	current := dummy

	for head != nil {
		if head.Val != val {
			current.Next = head
			head = head.Next
			current = current.Next
			current.Next = nil
		} else {
			head = head.Next
		}
	}

	return dummy.Next
}