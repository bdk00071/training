


// 反轉一個 list，每次搬到新 list 都加在 dummy 後面。


class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode *dummy = new ListNode, *tmp;

        while(head != NULL) {
            tmp = head;
            head = head->next;
            tmp->next = dummy->next;
            dummy->next = tmp;
        }
        
        return dummy->next;
    }
};
