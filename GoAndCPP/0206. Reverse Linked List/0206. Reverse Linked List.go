func reverseList(head *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy
	tmp := current

	for head != nil {
		tmp = head
		head = head.Next
		tmp.Next = dummy.Next
		dummy.Next = tmp
	}

	return dummy.Next
}