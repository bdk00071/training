/*
    題目不難，基礎解法只要把 int 放到其他 container 然後複製 reversed 的一份做比較就可以
    follow up  的條件是時間 O(n) & 固定空間
    
    幾步處理完：
    1, 算好有幾個 node
    2, 取出list後半段，數量是奇數的話不包括 middle node
    3, reverse the list that we have got in step 2
    4, 比較前半list與反轉過的後半list，有步相同就不是 Palindrome Linked List。middle node不需要處理。
*/


class Solution {
    ListNode* reverseList(ListNode* head) {
        ListNode* dummy = new ListNode(0), *tmp;

        while (head) {
            tmp = head;
            head = head->next;
            tmp->next = dummy->next;
            dummy->next = tmp;
        }

        return dummy->next;
    }

public:
    bool isPalindrome(ListNode* head) {
        int nCount = 0;
        ListNode *current = head, *pre = head;

        while (current) {
            current = current->next;
            nCount++;
        }

        if (nCount%2) nCount = nCount/2;
        else nCount = nCount/2-1;

        while (nCount != 0) {
            pre = pre->next;
            nCount--;
        }

        current = pre->next;
        pre->next = NULL;
        current = reverseList(current);

        while (current && head) {
            if (current->val != head->val) return false;
            current = current->next;
            head = head->next;
        }

        return true;
    }
};