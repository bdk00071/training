func reverseList(head *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy
	tmp := current

	for head != nil {
		tmp = head
		head = head.Next
		tmp.Next = dummy.Next
		dummy.Next = tmp
	}

	return dummy.Next
}

func isPalindrome(head *ListNode) bool {
	slow, fast := head, head

	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}

	slow = reverseList(slow)
	for slow != nil {
		if slow.Val != head.Val {
			return false
		}
		slow = slow.Next
		head = head.Next
	}

	return true
}