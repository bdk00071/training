// 這題蠻有趣，一開始拿到題目時不知道想幹什麼：傳給我的 node 就是要刪掉？？
// 仔細想才連上之前 vector 刪 val 的題目：把後面沒有要刪的搬過來。


class Solution {
public:
    void deleteNode(ListNode* node) {
        
        if (node->next->next == NULL) {
            node->val = node->next->val;
            node->next = NULL;
            return;
        } else {
            while (node->next->next != NULL) {
                node->val = node->next->val;
                node = node->next;
            }
        }
        node->val = node->next->val;
        node->next = NULL;
    }
};
