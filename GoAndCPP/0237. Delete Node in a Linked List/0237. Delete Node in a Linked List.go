func deleteNode(node *ListNode) {
	for true {
		if node.Next != nil {
			node.Val = node.Next.Val
		}
		if node.Next.Next == nil {
			node.Next = nil
			break
		}
		node = node.Next
	}
}