
// 前面有一題用 value 分前後的，這次用基偶數。



class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        bool bEven = true;
        ListNode *dummyOdd = new ListNode, *currentOdd = dummyOdd;
        ListNode *dummyEven = new ListNode, *currentEven = dummyEven;
        ListNode *tmp, *current;

        while (head) {
            if (bEven) current = currentEven;
            else current = currentOdd;
            bEven = !bEven;

            current->next = head;
            head = head->next;
            current = current->next;
            current->next = NULL;

            if (!bEven) currentEven = currentEven->next;
            else currentOdd = currentOdd->next;
        }

        tmp = dummyEven->next;
        while(tmp != NULL && tmp->next != NULL) tmp = tmp->next;
        if (tmp != NULL) tmp->next = dummyOdd->next;

        return dummyEven->next;
    }
};
