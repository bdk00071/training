func oddEvenList(head *ListNode) *ListNode {
	var idx int = 0
	oddDummy, evenDummy := new(ListNode), new(ListNode)
	oddCurrent, evenCurrent := oddDummy, evenDummy

	for head != nil {
		if idx%2 == 0 {
			evenCurrent.Next = head
			head = head.Next
			evenCurrent = evenCurrent.Next
			evenCurrent.Next = nil
		} else {
			oddCurrent.Next = head
			head = head.Next
			oddCurrent = oddCurrent.Next
			oddCurrent.Next = nil
		}
		idx++
	}

	evenCurrent.Next = oddDummy.Next
	return evenDummy.Next
}
