
// 跟第二題不同，位數是反的所以拿 stack 輔助
// 最優版可以不需要 new ListNode

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        if (!l1) return l2;
        else if (!l2) return l1;

        int carry = 0;
        stack<int> st1, st2, st3;
        ListNode *cur, *newHead;

        cur = l1;
        while (cur) {
            st1.push(cur->val);
            cur = cur->next;
        }
        cur = l2;
        while (cur) {
            st2.push(cur->val);
            cur = cur->next;
        }
        while (!st1.empty() || !st2.empty() || carry) {
            if (!st1.empty()) {
                carry += st1.top();
                st1.pop();
            }
            if (!st2.empty()) {
                carry += st2.top();
                st2.pop();
            }
            st3.push(carry%10);
            if (carry >= 10) carry = 1;
            else carry = 0;
        }
        cur = (l1 == NULL) ? l1 : l2;
        newHead = cur;
        while (!st3.empty()) {
            if (cur->next == NULL) cur->next = new ListNode;
            cur = cur->next;
            cur->val = st3.top();
            st3.pop();
        }
        return newHead->next;
    }
};
