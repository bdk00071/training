/**
  reverse 後比較好做事
*/

func reverse(head *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy
	tmp := current

	for head != nil {
		tmp = head
		head = head.Next
		tmp.Next = dummy.Next
		dummy.Next = tmp
	}

	return dummy.Next
}

func add(l1 *ListNode, l2 *ListNode) *ListNode {
	nCarry := 0
	dummy := new(ListNode)
	current := dummy

	for l1 != nil || l2 != nil {
		if l1 != nil && l2 != nil {
			l1.Val = l1.Val + l2.Val + nCarry
			nCarry = l1.Val / 10
			l1.Val %= 10
			current.Next = l1
		} else if l1 != nil {
			l1.Val = l1.Val + nCarry
			nCarry = l1.Val / 10
			l1.Val %= 10
			current.Next = l1
		} else if l2 != nil {
			l2.Val = l2.Val + nCarry
			nCarry = l2.Val / 10
			l2.Val %= 10
			current.Next = l2
		}
		if l1 != nil {
			l1 = l1.Next
		}
		if l2 != nil {
			l2 = l2.Next
		}
		current = current.Next
	}
	if nCarry != 0 {
		current.Next = new(ListNode)
		current = current.Next
		current.Val = nCarry
	}
	return dummy.Next
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	l1 = reverse(l1)
	l2 = reverse(l2)
	l1 = add(l1, l2)
	return reverse(l1)
}