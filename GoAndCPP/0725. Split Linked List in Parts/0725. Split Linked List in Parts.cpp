
/*
    要把 list 分成 k 組放 vector 中
    nodes 數量差距不能超過 1
    所以先算每條 list 要有幾個 nodes，還有前面幾個 lists 會比較多 node
    數量算好之後就是怎麼放到 vector<ListNode*> ans 裡面的方式
    假設 head 是第一個且不為 null
    那動作必是計算這次要放多少個 node 進去然後就把 head 放入 ans
    接下來因為我們已知計算的數字不會讓 head == NULL
    就不斷往後移動直到 nTake 是 1 為止
    如果此時 head == NULL 表示已放入剛好的數量
    若 head != NULL 那需要處理 head 與 head->next 的關聯

    這個大 while 完成後開始處理 ans
    因為答案要求 return 的 vector 要有 k 個 element
    但如果參數 head 只有 3 nodes 但 k > 3 就滿足不了這個條件
    所以後面多加 while 處理這件事
*/


class Solution {
public:
    vector<ListNode*> splitListToParts(ListNode* head, int k) {
        int nTotal = 0, nOneMore, nBasic, nTake;
        ListNode *current = head, *dummy = new ListNode;
        vector<ListNode*> ans;

        while (current) {
            nTotal++;
            current = current->next;
        }
        nOneMore = nTotal % k;
        nBasic = nTotal / k;
        current = dummy;
        while (head) {
            nTake = nBasic;
            if (nOneMore) {
                nTake++;
                nOneMore--;
            }
            
            ans.push_back(head);
            while (nTake!=1) {
                head = head->next;
                nTake--;
            }
            if (head != NULL) {
                current = head;
                head = head->next;
                current->next = NULL;
            }
        }
        while (ans.size() != k) ans.push_back({});
        
        return ans;
    }
};