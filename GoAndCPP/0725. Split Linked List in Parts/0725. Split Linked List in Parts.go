


	func splitListToParts(head *ListNode, k int) []*ListNode {
		total := listSize(head)
		basic, extra := total/k, total%k

		ans := make([]*ListNode, k)
		var prev, current *ListNode = nil, head

		for i := 0; i < k; i++ {
			ans[i] = current

			size := basic
			if extra > 0 {
				size++
				extra--
			}

			for j := 0; j < size; j++ {
				prev, current = current, current.Next
			}

			if prev != nil {
				prev.Next = nil
			}
		}
		return ans
	}

	func listSize(node *ListNode) int {
		var nCount int = 0
		for node != nil {
			node, nCount = node.Next, nCount+1
		}
		return nCount
	}
