/*
    要取一半，那就是 slow & fast 雙指針上場時間
    比對後可知道 while 停下的條件
*/


class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        ListNode *f = head, *s = head;
        while (f != NULL && f->next != NULL) {
            s = s->next;
            f = f->next->next;
        }
        return s;
    }
};
