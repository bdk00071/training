/*
    不難，有兩條 dummy 跟 index 記要把 node 分到哪邊
    分出前後就可以重組
*/
class Solution {
public:
    ListNode* mergeInBetween(ListNode* list1, int a, int b, ListNode* list2) {
        int idx = -1;
        ListNode *dummyA = new ListNode(0), *dummyB = new ListNode(0);
        ListNode *currentA = dummyA, *currentB = dummyB;

        while (list1) {
            idx++;
            if (idx >= a && idx <= b) {
                list1 = list1->next;
            } else if (idx < a) {
                currentA->next = list1;
                list1 = list1->next;
                currentA = currentA->next;
                currentA->next = NULL;
            } else if (idx > b) {
                currentB->next = list1;
                list1 = list1->next;
                currentB = currentB->next;
                currentB->next = NULL;
            }
        }

        currentA->next = list2;
        while (list2->next != NULL) list2 = list2->next;
        list2->next = dummyB->next;

        return dummyA->next;
    }
};