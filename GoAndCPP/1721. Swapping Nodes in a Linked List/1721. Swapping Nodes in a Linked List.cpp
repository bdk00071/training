

class Solution {
public:
    ListNode* swapNodes(ListNode* head, int k) {
        ListNode *slow = head, *fast = head, *tmp;

        while (k != 1) {
            k--;
            fast = fast->next;
        }

        tmp = fast;

        while (fast->next != NULL) {
            slow = slow->next;
            fast = fast->next;
        }
        swap(slow->val, tmp->val);

        return head;
    }
};