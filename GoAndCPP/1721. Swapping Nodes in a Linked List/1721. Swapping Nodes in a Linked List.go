

func swapNodes(head *ListNode, k int) *ListNode {
	slow, fast, tmp := head, head, head

	for k != 1 {
		k--
		fast = fast.Next
	}
	tmp = fast
	for fast.Next != nil {
		slow = slow.Next
		fast = fast.Next
	}
	slow.Val, tmp.Val = tmp.Val, slow.Val

	return head
}
