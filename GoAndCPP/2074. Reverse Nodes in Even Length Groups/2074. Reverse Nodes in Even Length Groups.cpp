/**
    把 list 分 group，從一個 node 一次增加一個拆開
    是偶數的話 reverse 過後裝回。 return 這個 list

    概念不難，用兩個 dummy，第一個記著現在拿幾個 node，拿好決定要不要 reverse
    第二個是 return 用。
 */
class Solution {
    ListNode* reverse(ListNode* head) {
        ListNode *dummy = new ListNode(0), *current = dummy, *tmp;

        while (head) {
            tmp = head;
            head = head->next;
            tmp->next = dummy->next;
            dummy->next = tmp;
        }

        return dummy->next;
    }
public:
    ListNode* reverseEvenLengthGroups(ListNode* head) {
        int max = 1, nTake;
        ListNode *tmp = new ListNode(0), *tmpCurrent = tmp;
        ListNode *dummy = new ListNode(0), *current = dummy;


        while (head) {
            nTake = 0;
            tmp->next = NULL;
            tmpCurrent = tmp;
            
            // 一直拿到 max 個或者沒得拿了
            while (nTake < max && head != NULL) {
                nTake++;
                tmpCurrent->next = head;
                head = head->next;
                tmpCurrent = tmpCurrent->next;
                tmpCurrent->next = NULL;
            }

            if (nTake%2 == 0) {
                tmpCurrent = tmp->next;
                tmp->next = NULL;
                tmpCurrent = reverse(tmpCurrent);
                current->next = tmpCurrent;
            } else {
                current->next = tmp->next;
            }
            
            while (current->next != NULL) current = current->next;
            
            max++;
        }

        return dummy->next;
    }
};