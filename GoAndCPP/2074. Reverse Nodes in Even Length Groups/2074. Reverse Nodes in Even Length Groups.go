
func reverse(head *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy
	tmp := current

	for head != nil {
		tmp = head
		head = head.Next
		tmp.Next = dummy.Next
		dummy.Next = tmp
	}

	return dummy.Next
}

func reverseEvenLengthGroups(head *ListNode) *ListNode {
	var max, nTake int = 1, 0
	dummy, tmp := new(ListNode), new(ListNode)
	current, tmpCurrent := dummy, tmp

	for head != nil {
		nTake = 0
		tmp.Next = nil
		tmpCurrent = tmp

		for nTake < max && head != nil {
			nTake++
			tmpCurrent.Next = head
			head = head.Next
			tmpCurrent = tmpCurrent.Next
			tmpCurrent.Next = nil
		}

		if nTake%2 == 0 {
			tmpCurrent = tmp.Next
			tmp.Next = nil
			tmpCurrent = reverse(tmpCurrent)
			current.Next = tmpCurrent
		} else {
			current.Next = tmp.Next
		}
		for current.Next != nil {
			current = current.Next
		}
		max++
	}

	return dummy.Next
}
