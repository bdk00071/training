
/**
    看到這種 middle 題應該都能用 fast slow 處理
    fast 先往後走兩個，是空的或下個是 null 就能把 slow 下個刪掉
 */


class Solution {
public:
    ListNode* deleteMiddle(ListNode* head) {
        if (head->next == NULL) return NULL;
        ListNode *fast = head, *slow = head;

        while (true) {
            fast = fast->next->next;
            if (fast == NULL || fast->next == NULL) {
                slow->next = slow->next->next;
                break;
            }
            slow = slow->next;
        }
        return head;
    }
};