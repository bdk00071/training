
/**
  看到這種 middle 題應該都能用 fast slow 處理
  fast 先往後走兩個，是空的或下個是 null 就能把 slow 下個刪掉
*/

func deleteMiddle(head *ListNode) *ListNode {
	if head.Next == nil {
		return nil
	}
	fast, slow := head, head

	for true {
		fast = fast.Next.Next
		if fast == nil || fast.Next == nil {
			slow.Next = slow.Next.Next
			break
		}
		slow = slow.Next
	}
	return head
}
