
/*
    Linked list 分兩半然後找答案，但這樣速度慢
 */

class Solution {
    ListNode* reverse(ListNode* head) {
        ListNode* dummy = new ListNode(0), *tmp;

        while (head) {
            tmp = head;
            head = head->next;
            tmp->next = dummy->next;
            dummy->next = tmp;
        }

        return dummy->next;
    }
public:
    int pairSum(ListNode* head) {
        int nCount = 0, nAns = 0;
        ListNode *current = head, *tmp;

        while (current) {
            current = current->next;
            nCount++;
        }

        current = head;
        nCount = nCount/2-1;
        while (nCount) {
            current = current->next;
            nCount--;
        }
        tmp = current;
        current = current->next;
        tmp->next = NULL;
        current = reverse(current);

        while (head && current) {
            if (nAns < head->val + current->val) nAns = head->val + current->val;
            head = head->next;
            current = current->next;
        }

        return nAns;
    }
};