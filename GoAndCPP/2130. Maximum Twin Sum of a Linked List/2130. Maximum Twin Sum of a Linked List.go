
/*
   Linked list 分兩半然後找答案，但這樣速度慢
*/

func reverse(head *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy
	tmp := current

	for head != nil {
		tmp = head
		head = head.Next
		tmp.Next = dummy.Next
		dummy.Next = tmp
	}

	return dummy.Next
}

func pairSum(head *ListNode) int {
	var nCount, nAns int = 0, 0
	current, tmp := head, head

	for current != nil {
		current = current.Next
		nCount++
	}
	current = head
	nCount = nCount/2 - 1

	for nCount != 0 {
		current = current.Next
		nCount--
	}
	tmp = current
	current = current.Next
	tmp.Next = nil
	current = reverse(current)

	for head != nil && current != nil {
		if nAns < head.Val+current.Val {
			nAns = head.Val + current.Val
		}
		head = head.Next
		current = current.Next
	}

	return nAns
}
