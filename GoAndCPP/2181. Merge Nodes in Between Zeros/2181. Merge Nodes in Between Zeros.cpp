/*
    給 list，把 0 中間的 Nodes 數字加起來然後 0 去除掉
    注意頭尾都一定是 0 、沒有連續的 0、至少 3 個 Nodes
    不難，站好一個 0 後面的數字，然後找下個 0
    把中間的數字加起來就好
    使用固定空間、時間是 O(n)
 */


class Solution {
public:
    ListNode* mergeNodes(ListNode* head) {
        int tmp = 0;
        ListNode *current = head->next, *dummy = new ListNode(0), *ptr = dummy;

        while (current) {
            if (current->val == 0) {
                ptr->next = current;
                ptr = ptr->next;
                current = current->next;
                ptr->next = NULL;
                ptr->val = tmp;
                tmp = 0;
            } else {
                tmp += current->val;
                current = current->next;
            }
        }

        return dummy->next;
    }
};