

func mergeNodes(head *ListNode) *ListNode {
	var tmp int = 0
	dummy, current := new(ListNode), head.Next
	ptr := dummy

	for current != nil {
		if current.Val == 0 {
			ptr.Next = current
			ptr = ptr.Next
			current = current.Next
			ptr.Next = nil
			ptr.Val = tmp
			tmp = 0
		} else {
			tmp += current.Val
			current = current.Next
		}
	}
	return dummy.Next
}
