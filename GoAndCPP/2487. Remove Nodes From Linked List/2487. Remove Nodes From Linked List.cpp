/**
    先 reverse list 比較好做事
    用一個 int 記左邊最大的數字是多少
    如果現在的 head 合條件才放到 current 後面
    做完後把 list 轉回來就好
 */


 class Solution {
    ListNode* reverse(ListNode* head) {
        ListNode *dummy = new ListNode(0), *current = dummy, *tmp;
        while (head) {
            tmp = head;
            head = head->next;
            tmp->next = dummy->next;
            dummy->next = tmp;
        }
        return dummy->next;
    }
public:
    ListNode* removeNodes(ListNode* head) {
        int big = 0;
        ListNode *dummy = new ListNode(0), *current = dummy;

        head = reverse(head);

        while (head) {
            if (head->val >= big) {
                current->next = head;
                head = head->next;
                current = current->next;
                current->next = NULL;
                big = max(current->val, big);
            } else {
                head = head->next;
            }
        }
        current = dummy->next;
        dummy->next = NULL;
        return reverse(current);
    }
};