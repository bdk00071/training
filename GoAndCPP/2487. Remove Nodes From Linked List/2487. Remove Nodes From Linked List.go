/**
  先 reverse list 比較好做事
  用一個 int 記左邊最大的數字是多少
  如果現在的 head 合條件才放到 current 後面
  做完後把 list 轉回來就好
*/

func reverse(head *ListNode) *ListNode {
	dummy := new(ListNode)
	current := dummy
	tmp := current

	for head != nil {
		tmp = head
		head = head.Next
		tmp.Next = dummy.Next
		dummy.Next = tmp
	}

	return dummy.Next
}

func removeNodes(head *ListNode) *ListNode {
	nBig := 0
	dummy := new(ListNode)
	current := dummy

	head = reverse(head)
	for head != nil {
		if head.Val >= nBig {
			current.Next = head
			head = head.Next
			current = current.Next
			current.Next = nil
			if current.Val > nBig {
				nBig = current.Val
			}
		} else {
			head = head.Next
		}
	}
	current = dummy.Next
	dummy.Next = nil
	return reverse(current)
}
